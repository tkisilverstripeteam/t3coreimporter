(function($){
    $.entwine('t3coreimporter', function($) {
        $('.t3coreimporter-importer__actions button').entwine({
            onclick: function() {
                var url,
                    action,
                    connector,
                    debug = 1,
                    $btn = this,
                    $grid = $btn.closest('.ss-gridfield'),
                    $form = $btn.closest('form'),
                    data = $form.serialize();

                $btn.addClass('loading');
                data += "&" + encodeURIComponent($btn.attr('name')) + '=' + encodeURIComponent($btn.val());

                action = $form.attr('action');
                if(debug) {
                    action += "?XDEBUG_SESSION_START=netbeans-xdebug";
                }

                url = $.path.makeUrlAbsolute(
                    action,
                    $('base').attr('href')
                );
                $.post(url,data,function(data) {
                    console.log('t3coreimporteradmin success',data);
                })
                    .done(function(data) {
                        console.log('t3coreimporteradmin done',data);
                        var output = 'Result not available';
                        console.log(typeof(data));
                        if(typeof(data) === 'object') {
                            output = data['error'] || data['output'];
                            console.log(output);
                        }
                        $btn.closest('tr').find('.t3coreimporter-importer__actionresult').text(output);
                    })
                    .fail(function() {
                        console.log( "t3coreimporteradmin fail" );
                    })
                    .always(function() {
                        console.log( "t3coreimporteradmin always" );
                        $btn.removeClass('loading');
                    });

                    }
         
            
        });
      
      
      
        

     
    
  });
})(jQuery);

