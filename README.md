# README #

This module migrates core Typo3 data to SilverStripe.

Built for SilverStripe 3.x.
NB: Work still in progress!

## Data Migration
For specifics of what Typo3 data is migrated and retained, refer to the following documentation: [Data Migration](docs/en/migration.md)

## Typo3 Schema
For an overview of the Typo3 core schema, refer to the following documentation: [Typo3 Schema Reference](docs/en/tables.md)
