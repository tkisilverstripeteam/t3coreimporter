<% if DeletedMessages %>
======================================
Deleted
======================================
$DeletedMessages
<% end_if %>
<% if CreatedMessages %>
======================================
Created
======================================
$CreatedMessages
<% end_if %>
<% if UpdatedMessages %>
======================================
Updated
======================================
$UpdatedMessages
<% end_if %>
<% if SkippedMessages %>
======================================
Skipped
======================================
$SkippedMessages
<% end_if %>