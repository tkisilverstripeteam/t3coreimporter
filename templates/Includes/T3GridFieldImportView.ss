<h2>Importers</h2>
<table class="t3coreimporter-table">
    <thead>
        <tr class="t3coreimporter-importer__headings">
            <th>Importer</th>
            <th>Table</th>
            <th>Statistics</th>
            <th>Results</th>
            <th>Actions</th>
        </tr>
    </thead>
    <tbody>
        <% loop $items %>
        <tr id="t3coreimporter-$ClassName">
            <td class="t3coreimporter-importer__title"><strong>$Title</strong></td>
            <td class="t3coreimporter-importer__table"><strong>$Table</strong></td>
            <td class="t3coreimporter-importer__stats">
                $Statistics.Raw
            </td>
            <td class="t3coreimporter-importer__results">
                $Results.Raw
                <div class="t3coreimporter-importer__actionresult" style="font-weight: bold;"></div>
            </td>
            <td class="t3coreimporter-importer__actions">
                <button type="submit" name="action_doImport" class="action nolabel ss-ui-button ui-button ui-widget ui-state-default ui-corner-all ui-button-text-icon-primary" title="Import" value="$ClassName" role="button" aria-disabled="false">
                    <span class="ui-button-icon-alternate ui-button-icon-primary ui-icon btn-icon-disk" style="display: inline-block;"></span>
                    <span class="ui-button-text" style="display: inline-block;">Import</span>
                </button>
				<button type="submit" name="action_doClear" class="action nolabel ss-ui-button ui-button ui-widget ui-state-default ui-corner-all ui-button-text-icon-primary" title="Clear" value="$ClassName" role="button" aria-disabled="false">
                    <span class="ui-button-icon-alternate ui-button-icon-primary ui-icon btn-icon-disk" style="display: inline-block;"></span>
                    <span class="ui-button-text" style="display: inline-block;">Clear</span>
                </button>

            </td>
        </tr>
        <% end_loop %>
    </tbody>
</table>
