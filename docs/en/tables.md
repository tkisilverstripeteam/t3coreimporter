# Typo3 Schema Reference

## Core fields
The following are common Typo3 fields which are used in a number of tables. The data in these columns are preserved and/or mapped to new columns or are used in the migration logic.

| Column | Description | Migration |
| :----- | :---------- | :-------- |
| *uid* | ID | Preserved as *T3_uid*: used for reference when re-establishing relations |
| *pid* | ID of page record is associated with | Preserved as *T3_pid*: used to find and insert new *PageID* |
| *crdate* | Creation timestamp | Mapped to *Created* |
| *cruser_id* | ID of user who created the record | Preserved: used to find and insert new *UserID*  |
| *tstamp* | Last updated timestamp | Mapped to *LastEdited* |
| *hidden* | Enabled/disabled boolean | Mapped to *Disabled* |
| *sorting* | Sorting value | Mapped to *Sort* |
| *starttime* | Enabled start time | Preserved and mapped in some cases |
| *endtime* | Enabled end time | Preserved and mapped in some cases |
| *deleted* | Deleted boolean | Deleted records are not migrated |

## Content table (sys_file)
| Column | Description | Migration |
| :----- | :---------- | :-------- |
| *last_indexed* |   | N/A |
| *missing* |   | N/A |
| *storage* | Storage / file system | N/A - All files use storage root of ASSETS_DIR |
| *type* | File type  | N/A |
| *metadata* |   | N/A |
| *identifier* | Filename with path relative to storage root | Mapped to *Filename* |
| *identifier_hash* | Filename hash  | N/A |
| *folder_hash* |   | N/A |
| *extension* | File extension  | N/A |
| *mime_type* | Desc  | N/A |
| *name* | File name  | Mapped to *Name* |
| *sha1* | File hash  | N/A |
| *size* | File size  | N/A |
| *creation_date* | File creation date  | N/A |
| *modification_date* | File modification date  | N/A |

## Groups table (be_groups)
Back end groups may be imported from Typo3.
In Typo3, groups may be linked to "subgroups" to inherit permissions from, but are not hierarchical. This differs from the SilverStripe hierarchical structure, so permissions and inheritance need to be setup after importing the groups into SilverStripe. It is also recommended to use roles when setting the permissions.

| Column | Description | Migration |
| :----- | :---------- | :-------- |
| *title* | Group title  | Mapped to *Title* |
| *description* | Group description  | Mapped to *Description* |
| *hidden* | Enabled/disabled boolean  | Mapped to *Disabled* |
| *pagetypes_select* | Page types users can select | __Todo__: Map? |
| *non_exclude_fields* | Description  | N/A |
| *db_mountpoints* | Page mount points  | N/A |
| *tables_select* | Tables which group may view  | N/A |
| *tables_modify* | Tables which group may edit  | N/A |
| *groupMods* | Back end modules group has access to | N/A |
| *file_mountpoints* | File mount points  | N/A |
| *inc_access_lists* | | N/A |
| *lockToDomain* | The host name from which the user is forced to login  | N/A |
| *TSconfig* | Additional configuration via TS  | N/A |
| *subgroup* | Groups to inherit settings from | N/A |
| *hide_in_lists* | Boolean preventing the user group from showing up in lists  | N/A |
| *explicit_allowdeny* | Fields allowed/denied to group members | N/A |
| *allowed_languages* | Which record languages the group members are limited to edit  | N/A |
| *custom_options* |   | N/A |
| *workspace_perms* | Workspace permissions  | N/A |
| *fileoper_perms* |  | N/A |
| *file_permissions* | File operation permissions  | N/A |
| *category_perms* | Category permissions  | N/A |

## Users table (be_users)

| Column | Description | Migration |
| :----- | :---------- | :-------- |
| *password* | Hashed password  | Replaced with *Password*. Users must set new password. |
| *admin* | Administrator flag  | Added to Administrators group |
| *usergroup* | Groups user is member of  | Used for creating Group_Members records, mapped to new groups |
| *disable* | Temporarily disable the user from logging in  | Mapped to *Disabled* |
| *realName* | Full name of user | Mapped to *FirstName* and *Surname* |
| *lang* | Default language for the user | Migration to *Locale* not yet supported |
| *email* | User email  | Mapped to *Email* |
| *lastlogin* | Timestamp of last login | Mapped to *LastVisited* |
| *description* | User description  | Mapped to *Description* |
| *avatar* | Flag if user has avatar image. Image reference stored in sys_file_reference  | N/A |
| *username* | Username  | N/A |
| *db_mountpoints* | Page mount points  | N/A |
| *options* | Options determining whether the user should inherit page tree or folder tree mountpoints from member groups | N/A |
| *userMods* | Back end modules user has access to | N/A |
| *uc* |   |  |
| *file_mountpoints* | File mount points  | N/A |
| *fileoper_perms* |  | N/A |
| *lockToDomain* | The host name from which the user is forced to login  | N/A |
| *TSconfig* | Additional configuration via TS  | N/A |
| *createdByAction* | Internal  | N/A |
| *usergroup_cached_list* | Internal  | N/A |
| *disableIPlock* | Disables the lock of the backend users session to the remote IP number.  | N/A |
| *allowed_languages* | The languages the group members are limited to edit | N/A |
| *workspace_perms* | Workspace permissions  | N/A |
| *workspace_id* | Workspace ID  | N/A |
| *workspace_preview* |   | N/A |
| *file_permissions* | File operation permissions  | N/A |
| *category_perms* | Category permissions  | N/A |


## Pages table (pages)
| Column | Description | Migration |
| :----- | :---------- | :-------- |
| *starttime* | Enabled start time | Mapped to *PublishOnDate* |
| *endtime* | Enabled end time | Mapped to *UnPublishOnDate* |
| *hidden* | Whether the page is disabled in the front end  | Used in migration to determine whether page is published or not |
| *perms_userid* | Owner of page | N/A |
| *perms_groupid* | Group assigned to page  | Mapped to SiteTree_EditorGroups relation |
| *perms_user* | Permissions assigned to owner | N/A |
| *perms_group* | Permissions assigned to group  | N/A |
| *perms_everybody* | Permissions assigned to all logged in users | N/A. (Future: If set, *CanEditType* is set to 'LoggedIn') |
| *title* | Page title  | Mapped to *Title* |
| *doktype* | Integer designating page type (see [page types](migration.md#page-types)) | Used in migration to assign *ClassName* and map relevant properties |
| *TSconfig* | Typoscript configuration for page | N/A |
| *storage_pid* | General record storage page ID | N/A |
| *is_siteroot* | Page is the root of a site | Future feature: a new sub-site is created and child pages assigned the Subsite ID |
| *php_tree_stop* | Stops backend rendering of the page tree in navigation and lists. | N/A |
| *url* | URL for external link  | Mapped to ExternalURL in RedirectorPage  |
| *urltype* | Protocol for the URL | Incorporated into ExternalURL in RedirectorPage |
| *shortcut* | Page id of linked page  | Used to find _LinkToID_ for _ShortcutPage_ |
| *shortcut_mode* | Shortcut mode  | Used to determine _ShortcutMode_ for _ShortcutPage_ |
| *no_cache* | Flag for not caching the page  | N/A |
| *fe_group* | Restricts viewing to specified front end groups  | Used for page viewing permissions |
| *subtitle* | Page sub-heading  | Mapped to custom field *SubTitle* |
| *layout* | Front end layout | N/A |
| *target* | The default Link Target for menu items linking to this page. | N/A |
| *media* | Flag that files have been attached to the page  | N/A |
| *lastUpdated* | Last modified datetime for display in front end | Mapped to custom field *DateUpdated* |
| *keywords* | Meta tag keywords  | Obsolete - N/A |
| *cache_timeout* | Cache lifetime for the page | N/A |
| *newUntil* | The date at which this page is no longer considered new | N/A |
| *description* | Meta tag description | Mapped to *MetaDescription* |
| *no_search* | Boolean for excluding the page from search operations on the website | Mapped to *ShowInSearch* (reversed) |
| *SYS_LASTCHANGED* | Last updated by system | N/A |
| *abstract* | Page Abstract | Mapped to custom field *Abstract* |
| *module* | Front end plugin associated with the page | N/A |
| *extendToSubpages* | By default the Publication Date, Expiration Date, Usergroup Access Rights, and Page Visibility options only affect the page where they are set. If the option 'Extend to Subpages' is checked for the page, then all subpages are affected as well.  | N/A |
| *author* | The page author's name | Mapped to *AuthorName*, if not associated with a member via *Author*  |
| *author_email* | The page author's email | Mapped to *AuthorEmail*, if not associated with a member via *Author* |
| *nav_title* | Title for menu items in the front end | Mapped to *MenuTitle* |
| *content_from_pid* |   | N/A |
| *mount_pid* | Stores page ID of mount when page type is "Mount Point" | N/A |
| *alias* | Unique identification string for a page | Mapped to *URLSegment* |
| *nav_hide* | Determines whether page should be hidden from navigation menus  | Mapped to *ShowInMenus* (reversed) |
| *mount_pid_ol* | Show content for the current page or replace it completely with the Mounted Page content | N/A |
| *t3ver_oid* |   | N/A |
| *t3ver_id* |   | N/A |
| *t3ver_label* |   | N/A |
| *editlock* | Only admins can edit the page and its records  | N/A |
| *l18n_cfg* | Localisation config  | N/A |
| *fe_login_mode* | Allows the logins and other related featured to be disabled for this page | N/A |
| *t3ver_wsid* |   | N/A |
| *t3ver_state* |   | N/A |
| *t3ver_stage* |   | N/A |
| *t3ver_count* |   | N/A |
| *t3ver_tstamp* |   | N/A |
| *t3ver_swapmode* |   | N/A |
| *t3_origuid* |   | N/A |
| *t3ver_move_id* |   | N/A |
| *url_scheme* | HTTP protocol  | N/A? |
| *backend_layout* | Back end layout  | N/A |
| *backend_layout_next_level* | Back end layout for sub-pages  | N/A |
| *cache_tags* |   | N/A |
| *categories* | Categories associated with page  | Currently unsupported: Could be mapped to Taxonomies in SilverStripe (future) |
| *tsconfig_includes* | Typoscript includes  | N/A |

## Content table (tt_content)
| Column | Description | Migration |
| :----- | :---------- | :-------- |
| *CType* | Content type | Used for mapping/converting to appropriate content blocks (see [content types](migration.md#content-types)).|
| *header* | Content element heading  | Mapped to content block *Title* |
| *rowDescription* | Back end description of content | Mapped to custom field *Description* |
| *bodytext* | Main text for the content element | Mapped to *Content*. Converted as necessary |
| *image* | Flag that images are associated | Triggers look up of image relations |
| *imagewidth* | Front end width of image |  |
| *imageheight* | Front end height of image  |  |
| *imageorient* | Position of images relative to text | Used for mapping to appropriate block view |
| *imagecols* | Columns of image  | Used for block view options |
| *imageborder* | Image border enabled  | N/A - Should be set in CSS or on image record |
| *media* |   | N/A |
| *layout* | Content element layout  | N/A |
| *cols* | Table columns for table CType | Used for parsing / converting to HTML |
| *colPos* | The column position of the element | N/A - Could be mapped to *BlockArea* |
| *subheader* | Content element subheader - available on "header" type  | Mapped to *SubTitle* |
| *fe_group* | Restricts viewing to specified front end groups  | Not currently supported: Used for block viewing permissions (future) |
| *header_link* |   | N/A |
| *image_zoom* | Click to enlarge image (show original)  | Link may be set in BBMediaItem |
| *header_layout* | Heading level or hidden  | Converted to *TitleTag* |
| *menu_type* | Menu type for menu CType  | N/A |
| *list_type* | Stores plugin name for *list* (plugin) CType | Mapped to *Description* |
| *select_key* | Plugin select code  | N/A |
| *sectionIndex* |   | N/A |
| *linkToTop* |   | N/A |
| *file_collections* | List of files used with *uploads* (File Links) CType  | N/A |
| *filelink_size* |   | N/A |
| *filelink_sorting* |   | N/A |
| *target* |   | N/A |
| *date* |   | N/A |
| *recursive* |   | N/A |
| *sys_language_uid* |   | N/A |
| *tx_impexp_origuid* |   | N/A |
| *pi_flexform* | Extra fields and values relevant to the specific record | N/A |
| *accessibility_title* |   | N/A  |
| *accessibility_bypass* |   | N/A |
| *accessibility_bypass_text* |   | N/A |
| *l18n_parent* |   | N/A |
| *l18n_diffsource* |   | N/A |
| *selected_categories* | Categories associated with the content record | Currently **unsupported** |
| *category_field* |   | N/A |
| *table_caption* |   | N/A |
| *table_delimiter* | Table column delimiter?  | N/A |
| *table_enclosure* |   | N/A |
| *table_header_position* |   | N/A |
| *table_tfoot* |   | N/A |
| *header_position* |   | N/A |
| *table_bgColor* |   | N/A |
| *table_border* |   | N/A |
| *table_cellpadding* |   | N/A |
| *table_cellspacing* |   | N/A |


## Content table (sys_file_reference)
Mapped to BBMediaItem

| Column | Description | Migration |
| :----- | :---------- | :-------- |
| *uid_local* | sys_file id  | Migration |
| *uid_foreign* | sys_file id  | Migration |
| *sorting_foreign* |  Foreign table sorting   | Mapped to Sort |
| *tablenames* | foreign table (eg. tt_content, tx_wsflexslider_domain_model_image) | Migration |
| *title* | Image title  | Maps to Title |
| *description* | Image description  | Migration |
| *alternative* | Image alt text  | Mapped to Alt |
| *link* | link ID or external URL  | Mapped to LinkPage or LinkURL | Uses typical TypoLink format: UID/URL target cssclass(es) link params (space separated, items with spaces are enclosed in quotes. Eg:
34 _top "css classes" "Test title" "link params"

