## Migration options
#### Export rendered HTML from Typo3
Find Typo3 extension which publishes static pages. Publish locally and import via staticsiteimporter. Possibly modify Typo3 extension so it produces import friendly data such as selectors, IDs of files/images, or skips plugin output.

#### Scrape rendered content
Use staticsiteimporter to scrape HTML. Modify module to identify content elements and insert into matching content blocks.  
Pros: Also creates site tree.
Cons: Could lose relational data to assets. May not work for hidden, disabled, or migrating data such as news and events.

#### Connect to site ####
Use a database connection to import tables into SilverStripe, mapping / converting data while importing. Use an FTP connection to fetch images individually? Or copy image folders first?
Pros: Groups, Users, page and content data can largely be preserved. Original images can be imported, and resized in SilverStripe for better quality. Potentially easier to use.
Cons: More programming initially to setup.


##### Migration

Find/create Typo3 content exporter which generates the HTML for import into SilverStripe.
Use Typo3 exporter: Generate table HTML, store in CSV and import into TableBlock?
