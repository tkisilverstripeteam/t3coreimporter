# Data Migration


## Users and Groups


## Files

### Typo3 FAL

In Typo3, files are stored within the "fileadmin" folder. Users can browse, edit and upload files in the folders they have been given access to through user/group file mounts.
* Basic file data is stored in sys_file
* Meta data is stored in sys_file_metadata
* File to content record mapping happens in sys_file_reference. Title, alt text, description (caption), etc are stored here.
* Image display parameters such as width, height, position, etc are stored in the content record.

See: [Typo3 FAL](https://docs.typo3.org/typo3cms/FileAbstractionLayerReference/Architecture/Database/Index.html)

### SilverStripe assets
SilverStripe stores its files in the "assets" folder and corresponding file data in the "File" table.

See: [SilverStripe file management](https://docs.silverstripe.org/en/3/developer_guides/files/file_management/)


## Page types <a id="page-types"></a>
Pages in Typo3 are stored in the "pages" table. There are a number of core page types. The selected page type is stored in the "doktype" field. It may be one of the following:

#### Page
* Standard (1) - The default page type. (__Supported__: mapped to _Page_)
* Backend User Section (6) - "Such a page will appear in the frontend only for a specific group of backend users (which means you have to be logged into the backend to see such pages)." (__Unsupported__: mapped to _Page_)

#### Link
* Shortcut (4) - Links to file or another page, with four possible modes (_shortcut_mode_): (__Supported__: mapped to _ShortcutPage_)
  * Selected page (0)
  * First subpage of selected/current page (1)
  * Random subpage of selected/current page (2)
  * Parent page of selected/current page (3)
(_urltype_ is 1 - http://)

* Mount Point (7) - "A mount point lets you select any other page in the page tree. All child pages of the chosen page will appear as child pages of the mount point."* (__Unsupported__: mapped to _Page_)

* Link to External URL (3) - "This is similar to the "Shortcut" type but leads the user to a page on another web site."* Has five different protocols available (_urltype_): (__Supported__: mapped to _ExternalLinkPage_)
  * Auto (0)
  * http:// (1)
  * https:// (2)
  * ftp:// (3)
  * mailto: (4)

#### Special
* Folder (254) - "A folder-type page is simply a container. It will not appear in the frontend. It is generally used to store other types of records than pages or content elements." (__Unsupported__: not imported) Management of such records should be provided by a back end module)
* Recycler (255) - "This is similar to the 'Folder' type, but indicates that the content is meant for removal. However it offers no cleanup function. It is just a visual indication."* (__Unsupported__: not imported)
* Menu Separator (199) - "Creates a visual separation in the page tree and, if configured, also in the frontend navigation."* (__Unsupported__: not imported)

*Reference: [Typo3 page types](https://docs.typo3.org/typo3cms/EditorsTutorial/Pages/PageTypes/Index.html)

## Content <a id="content-types"></a>

### Common columns

* header_layout : Determines heading tag for header or if header will be hidden
  * h1 (1)
  * h2 (2)
  * h3 (3)
  * h4 (4)
  * h5 (5)
  * h6 (6)
  * hidden (100)
  
### Header
*CType*: header

##### Relevant columns   
Core content columns  
*subheader*: User supplied via varchar field

### Text & Image
*CType*: textpic  

##### Relevant columns   
*imagewidth*: User supplied via number field  
*imageheight*: User supplied via number field  
*imageorient*: User supplied via icon selection:  
 * 0 - above_center
 * 1 - above_right
 * 2 - above_left
 * 8 - below_center
 * 9 - below_right
 * 10 - below_left
 * 17 - intext_right
 * 18 - intext_left
 * 25 - intext_right_nowrap
 * 26 - intext_left_nowrap

*imageborder*: User enabled via checkbox  
*imagecols*: User supplied via number field  
*image_zoom*: User enabled via checkbox (Opened popup window) Deprecated?  

###### Unsupported
The following columns were deprecated and no longer used in Typo3 7 and/or Fluid Styled Content: i*mage_compression, image_effects, spaceBefore, spaceAfter, image_noRows, imagecaption, imagecaption_position, image_link, section_frame*

### Image
*CType*: image  
Migration: parsed and saved as HTML and media using *BlockTextMedia*.

### Text
*CType*: text  
Migration: parsed and saved as HTML in *BlockTextMedia*.

## Lists

### Bullet list
*CType*: bullets
Bullet list delimited by line breaks.
Migration: parsed and saved as HTML in *BlockTextMedia*.

### Table
*CType*: table  
Delimited values stored in *bodytext* column. Accessibility and CSV parsing fields and values stored in flex form XML in *pi_flexform* column.

### File links
*CType*: uploads  

## Special

### Special Menus
*CType*: menu
**Unsupported**

### Insert records
*CType*: shortcut
Provides shortcut to another content element, stored in *records* column.
Migration: May be able to map these to blocks.

### Insert plugin
*CType*: list
Plugin content element.
Migration: creates placeholder *Block* with Typo3 plugin name as *Title*. Manual intervention needed.

### Divider
*CType*: div
**Unsupported**: Use horizontal line tool in WYSIWYG editor

### HTML
*CType*: html
Migration: Mapped to *BlockHTML* class

## Forms

### Form
*CType*: mailform
**Unsupported**

### Search
*CType*: search
**Unsupported**
