<?php

/**
 * Management of T3 Data Sources and import process
 */
class T3CoreImporterAdmin extends ModelAdmin
{
	
	private static $url_segment = 't3coreimporter';

	/**
	 * @todo Allow entries to be linked/unlinked to directories, to be true many_many.
	 * Currently it only allows deletes, so operates more like has_many 
	 * @var array 
	 */
    private static $managed_models = array('T3DataConfig');

    private static $menu_title = 'Typo3 Importer';
	
    
    private static $importers = array(
        'T3FileImporter',
        'T3BackendGroupImporter', 
        'T3BackendUserImporter',
        'T3PageImporter',
        'T3ContentImporter',
        'T3FileReferenceImporter'
    );
    
	public function getEditForm($id = null, $fields = null) {
		
        $list = $this->getList();

        $fieldConfig = GridFieldConfig_RecordEditor::create($this->stat('page_length'))
            ->removeComponentsByType('GridFieldFilterHeader')
            ->removeComponentsByType('GridFieldExportButton')
            ->removeComponentsByType('GridFieldPrintButton')
            ->removeComponentsByType('GridFieldDetailForm')
            ->addComponent(new T3GridFieldImportForm())
            ->addComponents(new T3GridFieldImportButton());
        $fieldConfig->getComponentByType('T3GridFieldImportForm')->setItemRequestClass('T3GridFieldImportForm_ItemRequest');
		$listField = GridField::create(
			$this->sanitiseClassName($this->modelClass),
			false,
			$list,
			$fieldConfig
		);

		// Validation
		if(singleton($this->modelClass)->hasMethod('getCMSValidator')) {
			$detailValidator = singleton($this->modelClass)->getCMSValidator();
			$listField->getConfig()->getComponentByType('T3GridFieldImportForm')->setValidator($detailValidator);
		}

		$form = CMSForm::create(
			$this,
			'EditForm',
			new FieldList($listField),
			new FieldList()
		)->setHTMLID('Form_EditForm');
		$form->setResponseNegotiator($this->getResponseNegotiator());
		$form->addExtraClass('cms-edit-form cms-panel-padded center');
		$form->setTemplate($this->getTemplatesWithSuffix('_EditForm'));
		$editFormAction = Controller::join_links($this->Link($this->sanitiseClassName($this->modelClass)), 'EditForm');
		$form->setFormAction($editFormAction);
		$form->setAttribute('data-pjax-fragment', 'CurrentForm');

		$this->extend('updateEditForm', $form);

		return $form;
	}
	
}
