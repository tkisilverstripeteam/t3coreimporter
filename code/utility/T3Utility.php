<?php


class T3Utility {
    
    public $dbConfig;
    public $dbConnector;
    
    public function __construct($dbConfig,$dbConnector=null)
    {
        $this->dbConfig = $dbConfig;
        $this->dbConnector = $dbConnector;
    }
    
 
    /**
     * Get an item from an array or object using "dot" notation.
     *
     * @param  object|array $data
     * @param  string|int $key  (path with dot notation, or array path)
     * @param  mixed $default
     *
     * @return mixed
     */
    public static function data_get($data, $key, $default = null) {
        if (empty($data) || $key === '' || $key === null)
            return value($default);
        $object_get = function ($obj, $k) {
            return isset($obj->{$k}) ? $obj->{$k} : null;
        };
        $array_get = function ($arr, $k) {
            return (isset($arr[$k])) ? $arr[$k] : null;
        };

        $result = $data;
        $keys = is_array($key) ? $key : explode('.', $key);
        $numKeys = count($keys);
        $i = 1;
        foreach ($keys as $k) {
            if (is_object($result)) {
                $result = $object_get($result, $k);
            } elseif (is_array($result)) {
                $result = $array_get($result, $k);
            } else {
                $result = null;
                if ($i < $numKeys)
                    break;
            }
            ++$i;
        }
        return ($result !== null) ? $result : self::value($default);
    }
    
    public static function value($value) {
        return ($value instanceof Closure) ? $value() : $value;
    }
    
    public function convertLinkTags($value,$fileFolder)
    {
        $self = $this;
        $newValue = preg_replace_callback('/(<link)\s+(.*)>(.*)(<\/link>)/iU',
            function($matches) use($self,$fileFolder) {
                // Process attributes
                $parts = $this->decodelinkParts($matches[2]);
                // Link content
                $linkText = isset($matches[3]) ? $matches[3] : '';
                // Determine type
                if(preg_match('/([a-zA-Z0-9._%+-]+@[a-zA-Z0-9.-]+\.[a-zA-Z]{2,6})/',$parts['url'])) {
                    $type = 'Email';
                } else {
                    $type = 'Url';
                }
                $parts['url'] = $this->stripFragment($parts['url']);
                if(is_numeric($parts['url'])) {
                    $id = $self->findPageIdByUid($parts['url']);
                    if($id) {
                        $parts['url'] = "[sitetree_link,id=$id]";
                    }
                } elseif(strpos($parts['url'],'file:') === 0) {
                    $id = $self->findFileIdByUid($parts['url']);
                    if($id) {
                        $parts['url'] = "[file_link,id=$id]";
                    }
                }
                $tag =  $self->makeLinkTag($parts['url'], $type, $linkText, $parts['class'],$parts['target'],$parts['title']);
                return $tag;
        },$value);
    
        return $newValue;
    }
    
    public function convertAbsoluteUrls($value,$domains)
    {
        $self = $this;
        $domainRegex = $this->domainRegex($domains);
        $regex = '/"(https?:\/\/)?('. $domainRegex .')([^\s]+[\w]*)?"/i';
        
        $newValue = preg_replace_callback($regex,
            function($matches) {
                $relative = isset($matches[3]) ? $matches[3] : '/';
                if(preg_match('/(index.php)?\?id=(\d+)/i',$relative,$matches2)) {
                   if(!empty($matches2[2])) {
                        // Found uid, so find page and get new relative path
                        $page = $this->findPageByUid($matches2[2]);
                        if($page) {
                            $relative = $page->Link();
                        }
                   }
                }
                // Ensure leading slash
                if(substr($relative,0,1) !== '/') {
                    $relative = '/'.$relative; 
                }
                return '"'. $relative .'"';
        },$value);
    
        return $newValue;
    }
    
    /**
     * @todo - Must instantiate image and get tag (for RTE resized images - use width/height)
     * @param type $value
     * @param type $fileFolder
     * @return type
     */
    public function convertImageTags($value,$fileFolder,$stripInline=true)
    {
        $newValue = preg_replace_callback('/<img\s+([^>]*)\/?>/i',
            function($matches) use($fileFolder,$stripInline) {
                if(!isset($matches[1])) return $matches[0];
                $attrCount = preg_match_all('/([-a-zA-z0-9]+=[\'"][^\'"]*[\'"])/',$matches[1],$attrMatches);
                preg_match('/data-htmlarea-file-uid="(\d+)"/i',$matches[1],$uidMatches);
                $fileUid = !empty($uidMatches[1]) ? $uidMatches[1] : null;
                // Update image attributes
                if($attrCount) {
                    $newAttrs = array();
                    foreach($attrMatches[0] as $attr) {
                        // Update src
                        if(strpos($attr,'src=') === 0) {
                            $path = substr($attr,strpos($attr,'=')+1);
                            if(strpos($path,'_processed_') !== false && $fileUid) {
                                $file = $this->findT3FileByUid($fileUid);
                                if(!empty($file['identifier'])) {
                                    $path = '"fileadmin'. $file['identifier'] .'"';
                                }
                            }
                            $newAttrs[] = 'src='. str_replace('fileadmin/',$fileFolder,$path);
                        }
                        // Update classes
                        elseif(strpos($attr,'class=') === 0) {
                            $oldClasses = array('img-left','img-center','img-right');
                            $newClasses = array('left','center','right');
                            $newAttrs[] = str_replace($oldClasses,$newClasses,$attr);
                        }
                        // Remove unnecessary attributes
                        elseif($stripInline && (strpos($attr,'style=') === 0	// Remove inline image styles
                            || strpos($attr,'data-htmlarea-file-uid=') === 0)	// Remove uid
                        ) {
                            continue;
                        }
                        // Keep other attributes
                        else {
                            $newAttrs[] = $attr;
                        }
                    }
                } else {
                    $newAttrs = $matches[1];
                }

                return '<img '. implode(' ',$newAttrs) .' />';
        },$value);

        return $newValue;
    }
    
    public function convertBulletsToHtml($value)
    {
        $items = preg_split('/$\R?^/m',$value);
        $str = "<ul>\n";
        foreach($items as $item) {
            $str .= "\t<li>". trim(strval($item)) ."</li>\n";
        }
        $str .= "</ul>\n";
        return $str;
    }
    
    public function convertDelimitedTableToHtml($value,$flexForm)
    {
        $caption = $this->findFlexFormValue($flexForm,"//field[@index='acctables_caption']/value");
        $summary = $this->findFlexFormValue($flexForm,"//field[@index='acctables_summary']/value");
        $headerPos = $this->findFlexFormValue($flexForm,"//field[@index='acctables_headerpos']/value") ?: '';
        $class = $this->findFlexFormValue($flexForm,"//field[@index='acctables_tableclass']/value");
        $delimiter = $this->findFlexFormValue($flexForm,"//field[@index='tableparsing_delimiter']/value") ?: 124;

        $html = '<table class="'. $this->escapeAttributeValue($class) .'" summary="'. $this->escapeAttributeValue($summary) .'">'."\n";
        if(!empty($caption)) {
            $html .= "\t<caption>". htmlentities($caption) ."</caption>\n";
        }
        $rows = explode(PHP_EOL,$value);
        $rowIdx = 0;
        foreach($rows as $row) {
            $html .= "\t<tr>\n";
            $cells = explode(chr(intval($delimiter)),$row);
            $cellIdx = 0;
            foreach($cells as $cell) {
                if(($headerPos === 'top' && $rowIdx === 0) 
                    || ($headerPos === 'left' && $cellIdx === 0)) {
                    $tag = 'th';
                } else {
                    $tag = 'td';
                }
                $html .= "\t\t<$tag>". strip_tags(trim($cell),'<sup><b><i><strong>') ."</$tag>\n";
            }
            $html .= "\t</tr>\n";
            $rowIdx++;
        }
        $html .= "</table>\n";
        return $html;
    }
    
    public function convertTextToParagraphs($value)
    {
        $items = preg_split('/$\R?^/m',$value);
        $str = '';
        foreach($items as $item) {
            $str .= "\n<p>". trim(strval($item)) ."</p>\n";
        }
        return $str;
    }
    
    public function findFlexFormValue($flexForm,$path) 
    {
        $xml = new \SimpleXMLElement($flexForm);
        $result = $xml->xpath($path);
        return (is_array($result) && isset($result[0])) ? strval($result[0]) : strval($result);
    }

    public function escapeAttributeValue($value)
    {
        return htmlspecialchars($value,ENT_QUOTES,'UTF-8',false);
    }
    
    public function transformTimestamp($value)
    {
        $tz = defined('SS_DATABASE_TIMEZONE') ? SS_DATABASE_TIMEZONE : null;
        $tstamp = (int) $value;
        if($tstamp > 0) {
            $dt = new DateTime;
            $dt->setTimestamp($tstamp);
            if($tz) {
               $dt->setTimezone(new DateTimeZone($tz));
            }
            return $dt->format('Y-m-d H:i:s');
        } else {
            return null;
        }
    }

    public function decodelinkParts($link)
    {
        $link = trim($link);
        $segments = strlen($link) ? str_replace([ '\\\\', '\\"' ], [ '\\', '"' ], str_getcsv($link, ' ')) : [];
        $parts = [
            'url' => isset($segments[0]) ? trim($segments[0]) : '',
            'target' => (isset($segments[1]) && $segments[1] !== '-') ? trim($segments[1]) : '',
            'class' => (isset($segments[2]) && $segments[2] !== '-') ? trim($segments[2]) : '',
            'title' => (isset($segments[3]) && $segments[3] !== '-') ? trim($segments[3]) : '',
            'params' => (isset($segments[4]) && $segments[4] !== '-') ? trim($segments[4]) : '',
        ];
        return $parts;
    }
    
    public function stripFragment($value)
    {
        if(strpos($value,'#') !== false) {
            $value = substr($value,0,strpos($value,'#'));
        }
        return $value;
    }
    
    public function makeLinkTag($url,$type,$linkText,$class=null,$target=null,$title=null)
    {
        $str = '<a href="';
        if($type === 'Email') {
            $str .= 'mailto:';
        }

        $str .= $url .'" ';

        if($class) {
            $str .= 'class="'. Convert::raw2htmlatt($class) .'" ';
        }
        
        if($title) {
            $str .= 'title="'. Convert::raw2htmlatt($title) .'" ';
        }

        if($target && $target !== '-') {
            $str .= 'target="'. Convert::raw2htmlatt($target) .'" ';
        }

        $str .= '>'. $linkText .'</a>';
        return $str;
    }
    
    public function findT3FileByUid($uid)
    {
        $sql =  "SELECT * FROM sys_file WHERE uid=$uid;";
        $query = $this->dbConnector->executeQuery($sql);
        return $query->nextRecord();
    }
    
    public function findPageIdByUid($uid)
    {
        $obj = $this->findPageByUid($uid);
        return ($obj) ? $obj->ID : null;
    }
    
    public function findPageByUid($uid)
    {
        $obj = SiteTree::get()->filter(array(
            'T3_uid' => intval($uid),
            'T3DataConfigID' => $this->dbConfig->ID
        ))->first();
        return $obj;
    }
    
    public function findFileIdByUid($uid)
    {
        $uid = str_replace('file:','',$uid);
        $obj = File::get()->filter(array(
            'T3_uid' => intval($uid),
            'T3DataConfigID' => $this->dbConfig->ID
        ))->first();
        return ($obj) ? $obj->ID : null;
    }
    
    public function findOrMake($class,$attributes)
    {
        $obj = $class::get()->filter($attributes)->first();
        if(!$obj) {
            $obj = $class::create($attributes);
        }
        
        return $obj;
    }
    
    protected function domainRegex($domains)
    {
        if(!is_array($domains)) {
            $domainArr = [];
            foreach(explode(',',strval($domains)) as $domain) {
                $domainArr[] = trim($domain);
            }
            $domains = $domainArr;
        }

        $regex = implode('|',$domains);
        $regex = str_replace('.','\\.',$regex);
        return $regex;
    }
  
}
