<?php

class T3DatabaseConnector {
    
    protected $dbConfig;
    
    protected $db;
    
    public function __construct(T3DataConfig $dbConfig)
    {
        $this->dbConfig = $dbConfig;
        $this->connect();
    }
    
    /* 
    -------------------------------------------------------------------------
    | Getters / setters
    ------------------------------------------------------------------------- 
    */
    
    public function getDatabase()
    {
        if(!$this->db) {
            $this->connect();
        }
        return $this->db;
    }
    
    protected function connect()
    {
        $connectionName = 't3database-'. $this->dbConfig->DBName;
        
        DB::connect($this->getDatabaseParameters(),$connectionName);
        
        $this->db = DB::get_conn($connectionName);
    }
    
    protected function getDatabaseParameters()
    {
        return array(
            'type' => $this->dbConfig->DBType,
            'server' => $this->dbConfig->DBServer,
            'username' => $this->dbConfig->DBUsername,
            'password' => $this->dbConfig->DBPassword,
            'database' => $this->dbConfig->DBName
        );
    }
    
    /* 
    -------------------------------------------------------------------------
    | Queries
    ------------------------------------------------------------------------- 
    */
    
    
    public function baseSelectQuery($table)
    {
        return "SELECT * FROM ". $this->db->escapeString($table);
    }
    
    public function allRecords($table)
    {
       return $this->baseSelectQuery($table);
    }
    
    public function nonDeletedRecords($table)
    {
       $hasField = $this->db->getSchemaManager()->hasField($table,'deleted');
       if(!$hasField) {
           return $this->allRecords($table);
       }
       $sql = $this->baseSelectQuery($table);
       $sql = $this->addWhere($sql, 'deleted != 1');
   
       return $sql;
    }
    
    public function addWhere($sql,$clause)
    {
        if(stripos($sql,'WHERE') === false) {
            $sql .= " WHERE ";
        } else {
            $sql .= " AND ";
        }
        $sql .= $clause;
        return $sql;
    }
    
    public function executeQuery($sql)
    {
        return $this->db->query($sql);
    }
}
