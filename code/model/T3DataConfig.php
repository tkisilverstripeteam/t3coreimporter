<?php


class T3DataConfig extends DataObject {
    
    /**
	 * @config
	 */
	private static $singular_name = 'Typo3 Data Configuration';

	/**
	 * @config
	 */
	private static $plural_name = 'Typo3 Data Configurations';

    
    /**
     * @config
     * @var array
     */
    private static $summary_fields = array(
       'Title' => 'Title',
       'DBName' => 'Database',
       'DBServer' => 'Server',
       'Status' => 'Status'
    );
    
    private static $db = array(
        'Title' => 'Varchar(255)',
        'DBType' => "Enum('MySQLDatabase','MySQLDatabase')",
        'DBServer' => 'Varchar(20)',
        'DBUsername' => 'Varchar(100)',
        'DBPassword' => 'Varchar(200)',
        'DBName' => 'Varchar(100)',
        'Status' => "Enum('NotImported,InProgress,Imported,Completed','NotImported')",
        'FileFolder' => 'Varchar(255)',
        'Domains' => 'Varchar(255)',
        'SubsiteID' => 'Int',
        'ExcludePages' => 'Varchar',
        'ImportLog' => 'Text'
    );
    
    
    /* 
    -------------------------------------------------------------------------
    | Forms
    ------------------------------------------------------------------------- 
    */
    
    public function getCMSFields()
	{
        $fields = parent::getCMSFields();
        $fields->removeByName('ImportLog');
        $fields->removeByName('SubsiteID');
        if(class_exists('Subsite')) {
            $subsiteOptions = Subsite::get()->map('ID','Title');
            $fields->push(DropdownField::create('SubsiteID','Subsite',$subsiteOptions));
        }
        return $fields;
    }
    
    
    /* 
    -------------------------------------------------------------------------
    | Getters / setters
    ------------------------------------------------------------------------- 
    */
   
    
    
}