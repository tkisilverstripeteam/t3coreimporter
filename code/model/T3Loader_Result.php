<?php


class T3Loader_Result extends BetterBulkLoader_Result {
      
    public function deletedMessages()
    {
        return $this->makeLogString($this->deleted);
    }
    
    public function createdMessages()
    {
        return $this->makeLogString($this->created);
    }
    
    public function updatedMessages()
    {
        return $this->makeLogString($this->updated);
    }
    
    public function skippedMessages()
    {
        return $this->makeLogString($this->skipped);
    }
    
    protected function makeLogString($arr)
    {
        $str = '';
        foreach($arr as $item) {
            $str .= implode(' : ',$item) ."\n";
        }
        return $str;
    }
    
    public function templateData()
    {
        return ArrayData::create(array(
            'DeletedMessages' => $this->deletedMessages(),
            'CreatedMessages' => $this->createdMessages(),
            'UpdatedMessages' => $this->updatedMessages(),
            'SkippedMessages' => $this->skippedMessages()
        ));
    }
    
}

