<?php

class T3DataSource extends BulkLoaderSource {
    
    protected $queryResult;
    
    public function __construct($queryResult)
    {
        $this->queryResult = $queryResult;
        
    }
    
    public function getIterator()
    {
        return $this->queryResult;
    }
    
}
