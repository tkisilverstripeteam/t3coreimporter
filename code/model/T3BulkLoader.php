<?php

class T3BulkLoader extends BetterBulkLoader {
    
    protected $relationCreateDefault = false;
    
    /**
     * A closure to be run on the initial record.
     * @var Closure
     */
    public $initialRecordCallback;
    
    /**
     * A closure to determine objectClass.
     * @var Closure
     */
    public $createObjectCallback;
    
    /**
     * A closure to be run after each record has been written.
     * @var Closure
     */
    public $recordWrittenCallback;
    
    
    public function setObjectClass($class)
    {
        $this->objectClass = $class;
    }
    
    protected function processAll($filepath, $preview = false)
    {
        if (!$this->source) {
            user_error("No source has been configured for the bulk loader",
                E_USER_WARNING
            );
        }
        $results = T3Loader_Result::create();
        $iterator = $this->getSource()->getIterator();
        foreach ($iterator as $record) {
            $this->processRecord($record, $this->columnMap, $results, $preview);
        }
        
        return $results;
    }
    
    protected function processRecord($record, $columnMap, &$results, $preview = false)
    {
        if (!is_array($record) || empty($record) || !array_filter($record)) {
            $results->addSkipped("Empty/invalid record data.");
            return;
        }
        
        if (is_callable($this->initialRecordCallback)) {
            $callback  = $this->initialRecordCallback;
            $record = $callback($record);
            if(empty($record)) {
                $results->addSkipped("Record did not meet import criteria");
                return;
            }
        }
        
        //map incoming record according to the standardisation mapping (columnMap)
        $record = $this->columnMapRecord($record);
        //skip if required data is not present
        if (!$this->hasRequiredData($record)) {
            $results->addSkipped("Required data is missing.");
            return;
        }
        
        if (is_callable($this->createObjectCallback)) {
            $callback  = $this->createObjectCallback;
            $placeholder = $callback($record,$this->objectClass);
            //print("\nplaceholder: ". $placeholder->ClassName);
        } else {
            $class = $this->objectClass;
            $placeholder = class_exists($class) ? new $class() : null;
        }
        
        // Skip
        if(!is_object($placeholder)) {
            $results->addSkipped(sprintf("Object not created for uid %d using class: %s",T3Utility::data_get($record,'T3_uid'),$this->objectClass));
            return;
        }

        //populate placeholder object with transformed data
        foreach ($this->mappableFields_cache as $field => $label) {
            //skip empty fields
            if (!isset($record[$field])) {
                continue;
            }
            $this->transformField($placeholder, $field, $record[$field]);
        }
        //find existing duplicate of placeholder data
        $obj = null;
        $existing = null;
        if (!$placeholder->ID && !empty($this->duplicateChecks)) {
            $placeHolderData = $placeholder->getQueriedDatabaseFields();
            unset($placeHolderData['ID']);
            $data = $placeHolderData;
            //don't match on ID, ClassName or RecordClassName
            unset($data['ClassName'], $data['RecordClassName']);
            $existing = $this->findExistingObject($data);
            //print("\nexisting: ". $existing->ClassName ."\n");
            //print_r($data);
        }
        if ($existing) {
            $obj = $existing;
            $obj->update($placeHolderData); // Allow updating of classname (set on placeholder instanciation)
        } else {
            $obj = $placeholder;
        }

        //callback access to every object
        if (is_callable($this->recordCallback)) {
            $callback  = $this->recordCallback;
            $callback($obj, $record);
        }

        $changed = $existing && $obj->isChanged();
        //try/catch for potential write() ValidationException
        try {
            // write obj record
            $obj->write();
            
            if (is_callable($this->recordWrittenCallback)) {
                $callback  = $this->recordWrittenCallback;
                $callback($obj, $record);
            }
        
            //publish pages
            if ($this->publishPages && $obj instanceof SiteTree) {
                $obj->publish('Stage', 'Live');
            }

            // save to results
            if ($existing) {
                if ($changed) {
                    $results->addUpdated($obj);
                } else {
                    $results->addSkipped("No data was changed.");
                }
            } else {
                $results->addCreated($obj);
            }
        } catch (ValidationException $e) {
            $results->addSkipped($e->getMessage());
        }

        $objID = $obj->ID;
        // reduce memory usage
        $obj->destroy();
        unset($existingObj);
        unset($obj);
        
        return $objID;
    }
    
    
}
