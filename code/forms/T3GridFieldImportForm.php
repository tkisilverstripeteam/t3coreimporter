<?php

class T3GridFieldImportForm extends GridFieldDetailForm {
    
    protected $template = 'GridFieldDetailForm';
    
}

class T3GridFieldImportForm_ItemRequest extends GridFieldDetailForm_ItemRequest {
    
    private static $allowed_actions = array(
		'edit',
		'view',
        'import',
        'T3ImportForm',
        'doImport',
        'doClear'
	);

    protected $dbConnector;
    
    /* 
    -------------------------------------------------------------------------
    | Actions
    ------------------------------------------------------------------------- 
    */
     
    public function import($request) {
        // Permissions
        if(!Permission::check('ADMIN')) {
            return $this->httpError(403);
        }
        
        // Requirements
        $requirements = Requirements::css(T3COREIMPORTER_DIR . '/css/t3coreimporteradmin.css');
        $requirements = Requirements::javascript(T3COREIMPORTER_DIR . '/javascript/t3coreimporteradmin.js');
        
        // Form
		$form = $this->T3ImportForm($this->gridField, $request);
        $controller = $this->getToplevelController();
		$return = $this->customise(array(
			'Backlink' => $controller->hasMethod('Backlink') ? $controller->Backlink() : $controller->Link(),
			'ItemEditForm' => $form,
		))->renderWith($this->template);

		if($request->isAjax()) {
			return $return;
		} else {
			// If not requested by ajax, we need to render it within the controller context+template
			return $controller->customise(array(
				// TODO CMS coupling
				'Content' => $return,
			));
		}
	}
    
    public function T3ImportForm()
    {
        $toplevelController = $this->getToplevelController();
        if(empty($this->record)) {
			$noActionURL = $controller->removeAction($_REQUEST['url']);
			$toplevelController->getResponse()->removeHeader('Location');   //clear the existing redirect
			return $toplevelController->redirect($noActionURL, 302);
        }
        // Check permissions
        $canView = $this->record->canView();
        
        if(!$canView) {
			return $toplevelController->httpError(403);
		}
        
         // Data connection
        $this->dbConnector = new T3DatabaseConnector($this->record);
        
        $actions = FieldList::create(
           // FormAction::create('doTableImport','Do import')
            //    ->setUseButtonTag(true)
            //    ->addExtraClass('ss-ui-action-constructive')
            //    ->setAttribute('data-icon', 'accept')
        );
        $fields = FieldList::create();
        $fields->push(LiteralField::create('Table',$this->createTableData()));

        $form = Form::create(
            $this,
            'T3ImportForm',
            $fields,
            $actions
        );
        
        /*
         * Top controller
         */
        // TODO Coupling with CMS
		
		if($toplevelController && $toplevelController instanceof LeftAndMain) {
			// Always show with base template (full width, no other panels),
			// regardless of overloaded CMS controller templates.
			// TODO Allow customization, e.g. to display an edit form alongside a search form from the CMS controller
			$form->setTemplate('LeftAndMain_EditForm');
			$form->addExtraClass('cms-content cms-edit-form center');
			$form->setAttribute('data-pjax-fragment', 'CurrentForm Content');
			if($form->Fields()->hasTabset()) {
				$form->Fields()->findOrMakeTab('Root')->setTemplate('CMSTabSet');
				$form->addExtraClass('cms-tabset');
			}

			$form->Backlink = $this->getBackLink();
		}

		$cb = $this->component->getItemEditFormCallback();
		if($cb) $cb($form, $this);
		$this->extend("updateT3ImportForm", $form);
		return $form;
    }
    
    public function doImport($data, Form $form)
    {
        // Find specified table
        $importerClass = !empty($data['action_doImport']) ? trim($data['action_doImport']) : '';
        $importer = $this->findImporter($importerClass);
        $result = $importer->import();

        $response = new SS_HTTPResponse(json_encode($result,JSON_FORCE_OBJECT));
        $response->addHeader('Content-Type','application/json');
        return $response;
    }
    
    public function doClear($data, Form $form)
    {
        // Find specified table
        $importerClass = !empty($data['action_doClear']) ? trim($data['action_doClear']) : '';
        $importer = $this->findImporter($importerClass);
        $result = $importer->clear();

        $response = new SS_HTTPResponse(json_encode($result,JSON_FORCE_OBJECT));
        $response->addHeader('Content-Type','application/json');
        return $response;
    }
    
    protected function findImporter($importerClass)
    {
        $importers = Config::inst()->get('T3CoreImporterAdmin','importers');
        if(empty($importerClass)) {
            return $this->httpError(400, 'Importer not specified');
        }
        
        if(!in_array($importerClass,$importers)) {
            return $this->httpError(400, 'Importer not configured');
        }
    
        // Instantiate importer and import data
        $importer = new $importerClass($this->dbConnector,$this->record);
        
        if(!($importer instanceof T3ImporterInterface)) {
            return $this->httpError(400, 'Importer does not implement T3ImporterInterface');
        }
        
        return $importer;
    }
    
    /* 
    -------------------------------------------------------------------------
    | Template
    ------------------------------------------------------------------------- 
    */
    
    
    
    protected function createTableData()
    {
        $list = ArrayList::create();
        $importers = Config::inst()->get('T3CoreImporterAdmin','importers');
        if(is_array($importers)) {
            foreach($importers as $importerClass) {
               $importer = class_exists($importerClass) ? new $importerClass($this->dbConnector,$this->record) : null;
               if($importer) {
                   $list->push($importer);
               }
            }
        }
        
        $tplData = ArrayData::create(array(
            'RecordID' => $this->record->ID,
            'items' => $list
        ));
        return $tplData->renderWith('T3GridFieldImportView');
    }
}