<?php

class T3ImportedFileExtension extends DataExtension {

    public function validate(ValidationResult $validationResult)
    {
        if(empty($this->owner->Filename)) {
            $validationResult->error('Filename empty');
        }
        $path = $this->owner->getFullPath();
        if(!file_exists($path)) {
            $validationResult->error(sprintf("Did not find file: %s",$path));
        }
    }
}
