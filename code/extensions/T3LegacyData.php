<?php

class T3LegacyData extends DataExtension {

    private static $db = array(
        'T3_uid' => 'Int',              // Record ID
        'T3_pid' => 'Int',              // Page ID
        'T3_record' => 'Text'           // Serialized version of old record
    );

    private static $has_one = array(
      'T3DataConfig' => 'T3DataConfig'
    );

    public function updateCMSFields(FieldList $fields)
    {
        // Remove legacy data fields
        $fields->removeByName(['T3_uid','T3_pid','T3_record','T3DataConfigID']);
        
        $member = Member::currentUser();
        // Do not show legacy data for non admins or file / folder dataobjects
        if(!$this->owner->T3_uid || !Permission::check('ADMIN', 'any', $member)
          || $this->owner instanceof File || $this->owner instanceof Folder
        ) {
            return;
        }
        
        $legacyFields = array(
            NumericField::create('T3_uid','T3_uid')->performReadonlyTransformation(),
            NumericField::create('T3_pid','T3_pid')->performReadonlyTransformation(),
            TextAreaField::create('T3_record','T3_record')->setDisabled(true),
            NumericField::create('T3DataConfigID','T3DataConfigID')->performReadonlyTransformation()
        );

        $fields->findOrMakeTab(
			'Root.LegacyData', _t('T3LegacyData.LegacyDataTab','Legacy Data')
		);
        $fields->addFieldsToTab('Root.LegacyData',$legacyFields);
        
    }
    
}
