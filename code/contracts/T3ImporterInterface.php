<?php

interface T3ImporterInterface {
    
    /**
     * @return array 
     */
    public function import();
    
    /**
     * @return string
     */
    public function getTitle();
    
    /**
     * @return string
     */
    public function getClassName();
    
    /**
     * @return string
     */
    public function getTable();
    
    
    /**
     * @return string
     */
    public function getStatistics();
    
    
    /**
     * @return string
     */
    public function getResults();
    
}
