<?php


class T3BackendGroupImporter extends T3Importer {
    
    private static $object_class = 'Group';

    protected $skipDeleted = true;
    
    protected $t3Table = 'be_groups';
    
    /**
     * 
     * @return array
     */
    public function columnMap()
    {
        return array(
            'title' => 'Title',
            'description' => 'Description',
            'hidden' => 'Disabled',             // Disabled flag
            'sorting' => 'Sort',
            'cruser_id' => 'CreatorID'
        );
 
    }
    
    public function applyMappableFields()
    {
        parent::applyMappableFields();

        $this->loader->mappableFields['ParentID'] = 'ParentID';
    }
    
    protected function transforms()
    {
        $self = $this;

        return array();
        
    }
    
    protected function applyRecordCallback()
    {
        $self = $this;
        
        $this->loader->recordCallback = function($obj,$record) use($self) {
            // Set ParentID
            $group = Group::get()->filter('Code','back-end')->first();
            $obj->ParentID = ($group) ? $group->ID : null;
            
        };
    }
    
    
}
