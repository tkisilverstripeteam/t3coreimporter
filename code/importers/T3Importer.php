<?php

abstract class T3Importer extends ViewableData implements T3ImporterInterface {
    
    private static $casting = array(
		'Title' => 'Varchar',
        'Table' => 'Varchar',
        'ClassName' => 'Varchar',
        'Statistics' => 'HTMLText',
        'Results' => 'HTMLText'
	);
    
    /**
     * @config
     * @var string 
     */
    private static $object_class;
    
    /**
     * @config
     * @var string
     */
    private static $log_path = 't3coreimporter/logs';   // relative to assets directory
    
    /**
     * @config
     * @var string
     */
    private static $log_template = 'T3CoreImporterResultLog';
    
    /**
     * Time limit for import (in seconds)
     * @var int
     */
    private static $time_limit = 600;
    
    protected $t3Table;
    
    protected $dbConnector;
    
    protected $dbConfig;
    
    protected $source;
    
    protected $loader;
    
    protected $skipDeleted = true;
    
    protected $skipImported = false;
    
    protected $t3Utility;
    
    protected $currentTimestamp;
    
    public function __construct(T3DatabaseConnector $dbConnector, T3DataConfig $dbConfig)
    {
        $this->dbConnector = $dbConnector;
        $this->dbConfig = $dbConfig;
        $this->t3Utility = new T3Utility($dbConfig,$dbConnector);
        $this->currentTimestamp = time();
    }
 
    abstract public function columnMap();
    
    public function getTitle()
    {
        $class = get_class($this);
        return _t("$class.Title");
    }
    
    public function getTable()
    {
        return $this->t3Table;
    }
    
    public function getDbConnector()
    {
        return $this->dbConnector;
    }
    
    public function getClassName()
    {
        return Convert::raw2att(get_class($this));
    }
    
    /**
     * @return string
     */
    public function getStatistics()
    {
        $totalRecords = $this->doSourceQuery(false)->numRecords();
        $totalNonDeleted = $this->doSourceQuery(true)->numRecords();
        
        $out = sprintf(_t('T3Importer.TotalRecords','<strong>%d</strong> total Typo3 records found.') ,$totalRecords);
        $out .= '<br />';
        $out .= sprintf(_t('T3Importer.TotalNonDeleted','<strong>%d</strong> Typo3 records are not deleted and will be imported.') ,$totalNonDeleted);
        
        return $out;
    }
    
    /**
     * @return string
     */
    public function getResults()
    {
        $totalImported = $this->totalImported();
        return sprintf(_t('T3Importer.TotalImported','<strong>%d</strong> Typo3 records have already been imported.') ,$totalImported);
    }
    
    /* 
    -------------------------------------------------------------------------
    | Init
    ------------------------------------------------------------------------- 
    */
    
    /**
     * Initialise source
     */
    protected function initSource()
    {
        $query = $this->doSourceQuery($this->skipDeleted,$this->skipImported);

        $this->source = new T3DataSource($query);
    }

    public function doSourceQuery($skipDeleted=true,$skipImported=false)
    {
        if($skipDeleted) {
            $sql = $this->dbConnector->nonDeletedRecords($this->t3Table);
        } else {
            $sql = $this->dbConnector->allRecords($this->t3Table);
        }
        
        if($skipImported) {
            $class = Config::inst()->get(get_class($this),'object_class',Config::UNINHERITED);
            $importedUids = array_unique(DataObject::get($class)->filter([
               'T3DataConfigID' => $this->dbConfig->ID 
            ])->column('T3_uid'));
            if(!empty($importedUids)) {
                $sql = $this->dbConnector->addWhere($sql, "{$this->t3Table}.uid NOT IN (". implode(',',$importedUids)) .')';
            }
        }
        
        return $this->dbConnector->executeQuery($sql);
    }
    
 
    /**
     * Initialise loader
     */
    protected function initLoader()
    {
        $class = Config::inst()->get(get_class($this),'object_class',Config::UNINHERITED);
        $this->loader = T3BulkLoader::create($class);
    }
    
     /* 
    -------------------------------------------------------------------------
    | Stats 
    ------------------------------------------------------------------------- 
    */
    
    public function totalImported($filters=[])
    {
        
        $class = Config::inst()->get(get_class($this),'object_class');
        $classInstance = $class::create();
        // Query
        $queryFilters = [
            'T3DataConfigID' => $this->dbConfig->ID 
         ];
         if(is_array($filters)) {
             $queryFilters += $filters;
         }
            
        
        $out = '';
        if($class) {
            if(class_exists('Subsite') && $this->dbConfig->SubsiteID) {
                Subsite::$disable_subsite_filter = true;
                if($classInstance->hasDatabaseField('SubsiteID')) {
                    $queryFilters['SubsiteID'] = $this->dbConfig->SubsiteID;
                }
            }
            $query = $class::get()->filter($queryFilters);
            /*if($class === 'File') {
            echo "<pre>sql: {$query->sql()} \r\n class: $class  count: {$query->count()} T3DataConfigID: {$this->dbConfig->ID} Conf subsite:  {$this->dbConfig->SubsiteID} \r\n</pre>";
            }*/
            $out = $query->count();
            Subsite::$disable_subsite_filter = false;
        } else {
            error_log("SilverStripe class '$class' not found for table '{$this->t3Table}'");
            $out = 'n/a';
        }
        
        return $out;
    }
    /* 
    -------------------------------------------------------------------------
    | Main logic 
    ------------------------------------------------------------------------- 
    */
    
    /**
     * Import
     */
    public function import()
    {

        // Set execution time
        $limit = intval(Config::inst()->get(get_class($this),'time_limit') ?: 600);
        set_time_limit($limit);
        // Init
        $this->initSource();
        $this->initLoader();
        $this->loader->setSource($this->source);
        
        if(class_exists('Subsite') && $this->dbConfig->SubsiteID) {
            Subsite::$force_subsite = $this->dbConfig->SubsiteID;
            if(Subsite::currentSubsiteID() !== intval($this->dbConfig->SubsiteID)) {
                Subsite::changeSubsite($this->dbConfig->SubsiteID);
            }
        }
            
        // Loader setup
        $this->applyInitialRecordCallback();
        $this->applyColumnMap();
        $this->applyMappableFields();
        $this->applyTransforms();
        $this->applyCreateObjectCallback();
        $this->applyDuplicateChecks();
        $this->applyRecordCallback();
        $this->applyRecordWrittenCallback();

        // Run
        try {
            $result = $this->loader->load();
            $resultData = array(
                'output' => $result->getMessage()
            );
            $this->logResult($result);
        } catch (Exception $ex) {
            $resultData = array(
                'error' => $ex->getMessage()
            );
        }
        
        return $resultData;
    }
    
    
    /**
     * Import
     */
    public function clear($filters=[])
    {
        // Set execution time
        $limit = intval(Config::inst()->get(get_class($this),'time_limit') ?: 600);
        set_time_limit($limit);
        
        // Run
        try {
            $class = Config::inst()->get(get_class($this),'object_class',Config::UNINHERITED);
            $classes = ClassInfo::ancestry($class,true);
            $baseClass = array_shift($classes);
            $versioned = Object::has_extension($baseClass,'Versioned');

            if(class_exists('Subsite') && $this->dbConfig->SubsiteID) {
                Subsite::$force_subsite = $this->dbConfig->SubsiteID;
            }
            $queryFilters = [
               'T3DataConfigID' => $this->dbConfig->ID 
            ];
            if(is_array($filters)) {
                $queryFilters += $filters;
            }
                
            $query = DataObject::get($class)->filter($queryFilters);
            $deleted = 0;
            foreach($query as $obj) {
                if(!$obj->ID) continue;
                if($versioned) {
                    if(method_exists($obj,'doUnpublish')) {
                        $obj->doUnpublish();
                    } else {
                        $origStage = Versioned::current_stage();
                        Versioned::reading_stage('Live');
                        $clone = clone $obj;
                        $clone->delete();
                        Versioned::reading_stage($origStage);
                    }
                    $obj->delete();
                } else {
                    $obj->delete();
                }
                $deleted++;
            }
            $resultData = array(
                'output' => sprintf("%d records deleted",$deleted)
            );
        } catch (Exception $ex) {
            $resultData = array(
                'error' => $ex->getMessage()
            );
        }
        
        return $resultData;
    }
    
    protected function logResult($result)
    {
        // Log folder
        $folderPath = Config::inst()->get(get_class($this),'log_path');
        if(substr($folderPath,-1) !== '/') {
            $folderPath .= '/';
        }
        $folder = Folder::find_or_make($folderPath);
        
        // Log file
        $filename = date_format(new DateTime('now'),'Y-m-d_His') .'_'. get_class($this) .'.txt';
        $log = $folder->getRelativePath() . $filename;

        $file = File::create(array(
            'Filename' => $log,
            'Name' => basename($log),
            'ShowInSearch' => 0,
            'T3DataConfigID' => $this->dbConfig->ID,
            'ParentID' => $folder->ID
        ));
        // Create actual file
        $tpl = Config::inst()->get(get_class($this),'log_template');
        $template = new SSViewer($tpl);
        $content = $template->process($result->templateData());
   
        file_put_contents($file->getFullPath(),$content);
        
        // Write file database object
        $file->write();
        
    }
    
    /* 
    -------------------------------------------------------------------------
    | Mapping 
    ------------------------------------------------------------------------- 
    */
    
    /**
     * Creates core column map. 
     * ie. [Typo3 column] => [SilverStripe column]
     * @return array
     */
    public function coreColumnMap()
    {
        $columns = array(
            'uid' => 'T3_uid',                  // T3 record ID
            'pid' => 'T3_pid',                  // T3 page ID where record was stored
            'crdate' => 'Created',              // Creation date (requires mutator)
            'tstamp' => 'LastEdited',
            'T3DataConfigID' => 'T3DataConfigID',
            'T3_record' => 'T3_record'
        );
        // Subsite support
        if(class_exists('Subsite')) {
            $columns['SubsiteID'] = 'SubsiteID';
        }
        
        return $columns;
    }
    
    protected function applyInitialRecordCallback()
    {
        $self = $this;
        $this->loader->initialRecordCallback = function($record) use($self) {
            // JSON encode record
            $record['T3_record'] = json_encode($record,JSON_FORCE_OBJECT);
            // Set T3DataConfigID
            $record['T3DataConfigID'] = $self->dbConfig->ID;
            $record['SubsiteID'] = $self->dbConfig->SubsiteID;
            return $record;
        };
    }
    
    protected function applyColumnMap()
    {
        $this->loader->columnMap = array_merge($this->coreColumnMap(),$this->columnMap());
    }
    
    
    protected function applyMappableFields()
    {
        $columns = array_values($this->loader->columnMap);
        $this->loader->mappableFields = array_combine($columns,$columns);
        
    }
    
    protected function coreDuplicateChecks()
    {
        $baseClass = ClassInfo::baseDataClass($this->loader->objectClass);
        
        return array(
            'T3Data' => array(
                'callback' => function($fieldName,$newRecord) use($baseClass) {    
                    if(is_array($newRecord) && class_exists($baseClass)) {
                        return $baseClass::get()->filter(array(
                            'T3_uid' => $newRecord['T3_uid'],
                            'T3DataConfigID' => $newRecord['T3DataConfigID']
                        ))->first();
                    } else {
                        return null;
                    }
                }
            )
        );
    }
    
    protected function duplicateChecks()
    {
        return array();
    }
    
    protected function applyDuplicateChecks()
    {
        $this->loader->duplicateChecks = array_merge(
            $this->coreDuplicateChecks(),$this->duplicateChecks()
        );
    }
    
    /* 
    -------------------------------------------------------------------------
    | Transforms
    ------------------------------------------------------------------------- 
    */
    protected function transforms()
    {
        return array();
    }
    
    /**
     * Transforms for core data
     * @return array
     */
    protected function coreTransforms()
    {
        $self = $this;
        return array(
            'Created' => array(
                'callback' => function($value,$placeholder) use($self) {
                    return $self->t3Utility->transformTimestamp($value);
                }
            ),
            'LastEdited' => array(
                'callback' => function($value,$placeholder) use($self) {
                    return $self->t3Utility->transformTimestamp($value);
                }
            ),
            'CreatorID' => array(
                'callback' => function($value,$placeholder) use($self) {
                    $obj = Member::get()->filter(array(
                        'T3_uid'=> $value,
                        'T3DataConfigID' => $placeholder->T3DataConfigID
                        ))->first();
                    return ($obj) ? $obj->ID : null;
                }
            )
        );
        
    }
    
    protected function applyTransforms()
    {
        $this->loader->transforms = array_merge($this->coreTransforms(),$this->transforms());
    }
    
    protected function applyCreateObjectCallback()
    {
      
    }
    
    protected function applyRecordCallback()
    {
        $self = $this;
        // Create file path
        $this->loader->recordCallback = function($obj,$record) use($self) {
            // Ensure SubsiteID has been set
            if(class_exists('Subsite') && !empty($self->dbConfig->SubsiteID) && empty($obj->SubsiteID)) {
                $obj->SubsiteID = $self->dbConfig->SubsiteID;
            }
        };
   
    }
    
    protected function applyRecordWrittenCallback()
    {
        /* example 
        $self = $this;
        $this->loader->recordWrittenCallback = function($obj,$record) use($self) {
            
        };
        */
    }
   
}
