<?php


class T3FileReferenceImporter extends T3Importer {
    
    private static $object_class = 'BBMediaItem';
  
    protected $t3Table = 'sys_file_reference';
    
    public function doSourceQuery($skipDeleted=true,$skipImported=false)
    {
        $sql = $this->dbConnector->baseSelectQuery('sys_file_reference');
        
        $db = $this->dbConnector->getDatabase();
        $sql =  "SELECT sfr.*, c.imageborder, c.hidden as content_hidden FROM ". $db->escapeString($this->t3Table) ." sfr";
        // Join with tt_content to include imageborder column
        $sql .= " JOIN tt_content c ON (sfr.tablenames = 'tt_content' AND sfr.uid_foreign=c.uid AND c.deleted=0)";
        // Exclude deleted content
        if($skipDeleted) {
            $sql = $this->dbConnector->addWhere($sql, 'sfr.deleted != 1');
        }
        // Constrain to tt_content
        $sql = $this->dbConnector->addWhere($sql, "tablenames IN ('tt_content')");
        // Constrain to imported content (blocks
        $blockIDs = array_unique(Block::get()->filter([
               'T3DataConfigID' => $this->dbConfig->ID 
        ])->column('T3_uid'));

        $blockIDs = implode(',',$blockIDs);
        if(!empty($blockIDs)) {
            $sql = $this->dbConnector->addWhere($sql, "uid_foreign IN ($blockIDs)");
        }
        
        $this->extend('extendSourceQuery',$sql,$skipDeleted);
        return $this->dbConnector->executeQuery($sql);
    }
    
  
    /**
     * 
     * @return array
     */
    public function columnMap()
    {
        return array(
            'title' => 'Title',
            'description' => 'Description',
            'sorting_foreign' => 'Sort',
            'alternative' => 'Alt',
            'link' => 'Link',
            'uid_local' => 'InternalImageID',
            'uid_foreign' => 'BlockID',
            'hidden' => 'Disabled'
        );

    }
    
    protected function applyRecordCallback()
    {
        /* example  */
        $self = $this;
        $this->loader->recordCallback = function($obj,$record) use($self) {
            $obj->NoBorder = !$record['imageborder'];
        };
    }
   
    /* 
    -------------------------------------------------------------------------
    | Transforms
    ------------------------------------------------------------------------- 
    */
    
    public function transforms()
    {
        $self = $this;
        return array(
            'Link' => array(
                'callback' => function($value,$placeholder) use ($self) {
                    if(empty($value)) {
                        return;
                    }
                    // Decode link
                    $parts = $self->t3Utility->decodelinkParts($value);
                    // URL
                    if(is_numeric($parts['url'])) {
                        $placeholder->LinkPageID = (int) $self->t3Utility->findPageIdByUid($parts['url']);
                        $placeholder->LinkType = 'internal-page';
                    } else {
                        $placeholder->LinkURL = $parts['url'];
                        if(strpos($parts['url'],'fileadmin') !== false) {
                            $placeholder->LinkType = 'internal-file';
                        } else {
                            $placeholder->LinkType = 'external';
                        }
                    }
                    // Target
                    $placeholder->LinkTarget = $parts['target'];
                }
            ),
            'InternalImageID' => array(
                'callback' => function($value,$placeholder) use ($self) {
                   $obj = File::get()->filter(array(
                        'T3_uid'=> $value,
                        'T3DataConfigID' => $placeholder->T3DataConfigID
                        ))->first();
                    return ($obj) ? $obj->ID : null;
                }
            ),
            'BlockID' => array(
                'callback' => function($value,$placeholder) use ($self) {
                    
                   $obj = Block::get()->filter(array(
                        'T3_uid'=> $value,
                        'T3DataConfigID' => $placeholder->T3DataConfigID
                        ))->first();

                    return ($obj) ? $obj->ID : null;
                }
            )
        );
    }
    
    protected function applyRecordWrittenCallback()
    {
        /* example  */
        $self = $this;
        
        $this->loader->recordWrittenCallback = function($obj,$record) use($self) {
            // Associate block with page
            $block = Block::get()->filter([
                'ID'=> $obj->BlockID
            ])->first();

            // Write/publish block which will save/publish media items via SavedMediaItems property
            if($block) {
                $block->write();
                if(boolval(Versioned::get_versionnumber_by_stage('Block', 'Live', $block->ID))) {
                    $block->publish('Stage', 'Live');
                }
            }
        };
       
    }
    
    /* 
    -------------------------------------------------------------------------
    | Helpers
    ------------------------------------------------------------------------- 
    */
    
}
