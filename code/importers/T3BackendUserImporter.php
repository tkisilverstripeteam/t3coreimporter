<?php


class T3BackendUserImporter extends T3Importer {
    
    private static $object_class = 'Member';

    protected $t3Table = 'be_users';
    
    /**
     * 
     * @return array
     */
    public function columnMap()
    {
        return array(
            'email' => 'Email',
            'lastlogin' => 'LastVisited',
            'description' => 'Description',             
            'disable' => 'Disabled',
            'realName' => 'FirstName',
            'cruser_id' => 'CreatorID'
        );
        
    }
    
    protected function duplicateChecks()
    {
        return array('Email');
    }
    
    public function applyMappableFields()
    {
        parent::applyMappableFields();
        $this->loader->mappableFields['FirstName'] = 'First Name';
        $this->loader->mappableFields['Surname'] = 'Last Name';
        $this->loader->mappableFields['Creator'] = 'Creator';
        $this->loader->mappableFields['Groups'] = 'Groups';
    }
    
    protected function transforms()
    {
        $self = $this;
        
        return array(
            'FirstName' => array(
                'callback' => function($value,$placeholder) use($self) {
                    $firstName = null;
                    $lastName = null;
                    if(!empty($value)) {
                        $parts = preg_split("/[\s]+/",$value);
                        $firstName = array_shift($parts);
                        $lastName = implode(' ',$parts);
                        $placeholder->update([
                           'FirstName' => $firstName,
                           'Surname' => $lastName
                        ]);
                    }
                    return $firstName;
                }
            )
        );
        
    }
    
    
    protected function applyRecordWrittenCallback()
    {
        /* example  */
        $self = $this;
        
        $this->loader->recordWrittenCallback = function($obj,$record) use($self) {
            // Add to groups
            $t3GroupIds = explode(',',$record['usergroup']);
            $groups = Group::get()->filter('T3_uid',$t3GroupIds);
            foreach($groups as $group) {
                $obj->Groups()->add($group);
            }
        };
       
    }
    
}
