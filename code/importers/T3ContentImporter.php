<?php


class T3ContentImporter extends T3Importer {
    
    private static $object_class = 'Block';
    
    protected $t3Table = 'tt_content';
    
    protected $skipImported = true;
    
    /**
     * Map colPos column to blockarea name
     * @config
     * @var array
     */
    private static $map_colpos_blockarea = array(
        0 => 'MainContent'
    );
    
    public function doSourceQuery($skipDeleted=true,$skipImported=false)
    {
        $db = $this->dbConnector->getDatabase();
        $sql =  "SELECT tt_content.*, COUNT(sfr.uid) as image_count FROM ". $db->escapeString($this->t3Table);
        // Join with sys_file_reference
        $sql .= " LEFT JOIN sys_file_reference sfr ON (sfr.tablenames='tt_content' AND sfr.uid_foreign=tt_content.uid AND sfr.deleted=0)";
        
        // Exclude deleted content
        if($skipDeleted) {
            $sql = $this->dbConnector->addWhere($sql, 'tt_content.deleted != 1');
        }
        
        // Limit content to that belonging to successfully imported pages
        $pageClass = Config::inst()->get('T3PageImporter','object_class',Config::UNINHERITED);
        $importedPages = array_unique($pageClass::get()->filter([
               'T3DataConfigID' => $this->dbConfig->ID 
        ])->column('T3_uid'));
        
        if(empty($importedPages)) {
          $importedPages = array(0);
        }
        
        
        $sql = $this->dbConnector->addWhere($sql, "tt_content.pid IN (SELECT uid FROM pages WHERE uid IN (". implode(',',$importedPages) .'))');
        
        if($skipImported) {
            $class = Config::inst()->get(get_class($this),'object_class',Config::UNINHERITED);
            $importedUids = array_unique(DataObject::get($class)->filter([
               'T3DataConfigID' => $this->dbConfig->ID 
            ])->column('T3_uid'));
            if(!empty($importedUids)) {
                $sql = $this->dbConnector->addWhere($sql, "{$this->t3Table}.uid NOT IN (". implode(',',$importedUids)) .')';
            }
        }
        // Debug
        //$sql .= " AND ({$this->t3Table}.uid IN (". implode(',',[2773]) .')) '; //
        // Group
        $sql .= ' GROUP BY tt_content.uid ORDER BY tt_content.uid ASC';
        return $this->dbConnector->executeQuery($sql); 
    }
    
    /**
     * 
     * @return array
     */
    public function columnMap()
    {
        return array(
            'header' => 'Title',
            'header_layout' => 'TitleTag',
            'subheader' => 'SubTitle',
            'rowDescription' => 'Description',
            'bodytext' => 'Content',
            'sorting' => 'Sort',
            'tx_tkicustomcontent_type' => 'FxType',
            'tx_tkicustomcontent_open' => 'FxDefaultState'
        );

    }
    
    protected function applyCreateObjectCallback()
    {
        $self = $this;
        // Defaults
        $defaultClass = $this->loader->objectClass;
        $defaultTransforms = $this->loader->transforms;
        
        $this->loader->createObjectCallback = function($record) use($self,$defaultClass,$defaultTransforms) {
            
            /*
             * Determine object class, columnMap and mappableFields needed for record
             */
            $class = $defaultClass;
            $transforms = $defaultTransforms;
            $ctype = strtolower($record['CType']);
            // Set ClassName
            switch($ctype) {
                case 'header':
                case 'textpic':
                case 'text':  
                case 'image':
                case 'bullets':
                    $class = 'BlockTextMedia';
                    $method = 'transform'. ucfirst($ctype);
                    if(method_exists($self,$method)) {
                        $transforms['Content'] = $self->$method($record);
                    }
                    break;
                case 'table':
                    $class = 'BlockTextMedia';
                    $transforms['Content'] = $self->transformTable($record);
                    break;
                case 'html':
                    $class = 'BlockHTML';
                    if(method_exists($self,'transformHtml')) {
                        $transforms['Content'] = $self->transformHtml($record);
                    }
                    break;
                case 'shortcut':
                    // Map to block
                    break;
                // Unsupported
                case 'list':
                case 'mailform':
                case 'search':
                case 'div':
                case 'menu':
                case 'uploads':
                    // Use placeholder to provide information for manual intervention
                    $class = 'Block';
                    $transforms['Description'] = array(
                        'callback' => function($value,$placeholder) use ($self) {
                            $prepend = '(Content type: '. $record['CType'];
                            if($record['CType'] === 'list') {
                                $prepend .= ' / Plugin name: '. $record['list_type'];
                            }
                            $prepend .= ") \n";
                            return $prepend . $value;
                        }
                    );
                    return $class;
                    break;
            }
            // Update transforms
            $self->loader->transforms = $transforms;
            
            if(empty($class) || !class_exists($class)) {
                return null;
            }
            return new $class();
        };
    }
    
    protected function applyRecordCallback()
    {
        $self = $this;
        $this->loader->recordCallback = function($obj,$record) use($self) {
            $blockClass = get_class($obj);
            if($blockClass === 'BlockTextMedia') {
                // Assign text media properties
                $self->addTextMediaProperties($obj,$record);
            }
            
            // Ensure SubsiteID has been set
            if(class_exists('Subsite') && !empty($self->dbConfig->SubsiteID) && empty($obj->SubsiteID)) {
                $obj->SubsiteID = $self->dbConfig->SubsiteID;
            }
        };
    }
    
    protected function applyRecordWrittenCallback()
    {
        /* example  */
        $self = $this;
        
        $this->loader->recordWrittenCallback = function($obj,$record) use($self) {
            // Associate block with page
            $page = SiteTree::get()->filter('T3_uid',$record['T3_pid'])->first();
            if($page) {
                $blockArea = $self->determineBlockArea($record['colPos']);
               // echo "\n $obj->ID : $blockArea";
                $page->Blocks()->add($obj,array(
                    'Sort' => $record['Sort'],
                    'BlockArea' => $blockArea
                ));
            }
            
            // Publish block if not hidden and is current
            $hidden = intval($record['hidden']);
            $current = true;
            if((!empty($record['starttime']) && $record['starttime'] > $self->currentTimestamp)
                || (!empty($record['endtime']) && $record['endtime'] < $self->currentTimestamp)) {
                $current = false;
            }
            
            if(!$hidden && $current) {
                $obj->publish('Stage', 'Live');
            } 
            
        };
       
    }

    protected function addTextMediaProperties($obj,$record) {
        $imageorient = (int) $record['imageorient'];

        switch($imageorient) {
            case 1:
                $view = 'BBLayoutTextMediaAbove';
                $align = 'right';
                break;
            case 2:
                $view = 'BBLayoutTextMediaAbove';
                $align = 'left';
                break;
            case 8:
                $view = 'BBLayoutTextMediaBelow';
                $align = 'center';
                break;
            case 9:
                $view = 'BBLayoutTextMediaBelow';
                $align = 'right';
                break;
            case 10:
                $view = 'BBLayoutTextMediaBelow';
                $align = 'left';
                break;
            case 17:
                $view = 'BBLayoutTextMediaRightWrap';
                $align = 'right';
                break;
            case 18:
                $view = 'BBLayoutTextMediaLeftWrap';
                $align = 'left';
                break;
            case 25:
                $view = 'BBLayoutTextMediaRight';
                $align = 'right';
                break;
            case 26:
                $view = 'BBLayoutTextMediaLeft';
                $align = 'left';
                break;
            case 0:
            default:
                $view = 'BBLayoutTextMediaAbove';
                $align = 'center';
                break;
        }
        // Text only
        if(strtolower($record['CType'] === 'text')) {
           $view = 'BBLayoutTextMediaNone';
        }
        
        $obj->ViewClass = $view;
        $obj->MediaItemAlign = $align;
        $obj->MediaView = $this->determineMediaView($obj,$record);
        $obj->MediaItemWidth = $record['imagewidth'];
        $obj->MediaItemHeight = $record['imageheight'];

    }
    
    /**
     * 
     * Options are:
     * 'BBMediaSingleView',
	 * 'BBMediaRandomView',
	 * 'BBMediaMultipleVertical',
	 * 'BBMediaMultipleHorizontal',
	 * 'BBMediaSlideshow',
     * @param Block $obj
     * @param array $record
     * @return string
     */
    protected function determineMediaView($obj,$record) {
        if(intval($record['imagecols']) > 1) {
            return 'BBMediaMultipleHorizontal';
        } elseif(intval($record['image_count']) > 1) {
            return 'BBMediaMultipleVertical';
        }
        
        return 'BBMediaSingleView';
    }
    
    
    /* 
    -------------------------------------------------------------------------
    | Transforms
    ------------------------------------------------------------------------- 
    */
    
    public function transforms()
    {
        $self = $this;
        return array(
            'TitleTag' => array(
                'callback' => function($value,$placeholder) use ($self) {
                    $old = intval($value);
                    if(get_class($placeholder) !== 'BlockHTML' && 
                        (($old > 1 && $old < 7) || ($old == 0 && !empty($placeholder->Title)))) {
                        $level = $old ?: 2;
                        $value = "h{$level}";
                    } else {
                        $value = 'hidden';
                    }
                    return $value;
                }
            ),
            'tx_tkicustomcontent_open' => array(
                'callback' => function($value,$placeholder) use ($self) {
                    if(is_numeric($value)) {
                        $value = ($value) ? 'active' : 'inactive';
                    } else {
                        $value = null;
                    }
                    return $value;
                }
            )
        );
    }
    
    
    protected function transformTextpic($record)
    {
        $self = $this;
        $fileFolder = $this->getFileFolder();
        return array(
            'callback' => function($value,$placeholder) use ($self,$fileFolder) {
                $value = $self->t3Utility->convertImageTags($value, $fileFolder);
                $value = $self->t3Utility->convertLinkTags($value, $fileFolder);
                $value = $self->t3Utility->convertTextToParagraphs($value);
                return $value;
            }
        );
    }
    
    protected function transformText($record)
    {
        return $this->transformTextpic($record);
    }
   
    protected function transformHtml($record)
    {
        $self = $this;
        $fileFolder = $this->getFileFolder();
        $domains = $this->getDomains();
        return array(
            'callback' => function($value,$placeholder) use ($self,$fileFolder,$domains) {
                $value = $self->t3Utility->convertImageTags($value, $fileFolder);
                $value = $self->t3Utility->convertAbsoluteUrls($value,$domains);
                return $value;
            }
        );
    }
    
    protected function transformTable($record)
    {
        $self = $this;
        $fileFolder = $this->getFileFolder();
        $domains = $this->getDomains();

        return array(
            'callback' => function($value,$placeholder) use ($self,$fileFolder,$domains,$record) {
                $value = $self->t3Utility->convertImageTags($value, $fileFolder);
                $value = $self->t3Utility->convertAbsoluteUrls($value,$domains);
                $value = $self->t3Utility->convertDelimitedTableToHtml($value,$self->t3Utility->data_get($record,'pi_flexform'));
                return $value;
            }
        );
    }
    
    /**
     * Unset content
     * @param type $record
     * @return type
     */
    protected function transformHeader($record)
    {
        $self = $this;
        
        return array(
            'callback' => function($value,$placeholder) use ($self) {
                return null;
            }
        );
    }
    
    /**
     * Unset content
     * @param type $record
     * @return type
     */
    protected function transformImage($record)
    {
        $self = $this;
        
        return array(
            'callback' => function($value,$placeholder) use ($self) {
                return null;
            }
        );
    }
    
    protected function transformBullets($record)
    {
        $self = $this;
        
        return array(
            'callback' => function($value,$placeholder) use ($self) {
                $value = $self->t3Utility->convertBulletsToHtml($value);
                return $value;
            }
        );
    }
    
    /* 
    -------------------------------------------------------------------------
    | Helpers
    ------------------------------------------------------------------------- 
    */
    
    public function getDomains()
    {
        return $this->dbConfig->Domains;
    }
    
    protected function getFileFolder()
    {
        $fileFolder = $this->dbConfig->FileFolder ?: Config::inst()->get('T3FileImporter','default_folder');
        // Ensure trailing slash
        if(substr($fileFolder,-1) !== '/') {
            $fileFolder .= '/'; 
        }
        return $fileFolder;
    }
    
    protected function determineBlockArea($colPos)
    {
        $map = Config::inst()->get(get_class($this),'map_colpos_blockarea',Config::UNINHERITED);
        return (is_array($map) && isset($map[$colPos])) ? $map[$colPos] : null;
    }
    
}
