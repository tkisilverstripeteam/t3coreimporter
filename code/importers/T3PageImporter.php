<?php


class T3PageImporter extends T3Importer {
    
    /**
     * @config
     * @var string 
     */
    private static $object_class = 'Page';

    protected $t3Table = 'pages';
    
    private static $class_uid_map = [];

    protected $skipImported = true;
    
    public function doSourceQuery($skipDeleted=true,$skipImported=false)
    {
 
        $sql = $this->dbConnector->baseSelectQuery($this->t3Table);
        
        // Exclude deleted content
        if($skipDeleted) {
            $sql = $this->dbConnector->addWhere($sql, 'deleted != 1');
        }
        
        // Skip root / domain page
        if(!empty($this->dbConfig->ExcludePages)) {
            $sql = $this->dbConnector->addWhere($sql, 'uid NOT IN ('. $this->dbConfig->ExcludePages .')');
        }
        
        if($skipImported) {
            $class = Config::inst()->get(get_class($this),'object_class',Config::UNINHERITED);
            $importedUids = array_unique(DataObject::get($class)->filter([
               'T3DataConfigID' => $this->dbConfig->ID 
            ])->column('T3_uid'));
            if(!empty($importedUids)) {
                $sql = $this->dbConnector->addWhere($sql, "{$this->t3Table}.uid NOT IN (". implode(',',$importedUids)) .')';
            }
        }
        // Debugging
        //$sql .= " AND ({$this->t3Table}.uid IN (". implode(',',[1177]) .')) '; //
        //$sql .= "OR {$this->t3Table}.doktype IN (". implode(',',[3,4]) .'))';
        //$sql .= ' ORDER BY pid ASC, sorting ASC';
        return $this->dbConnector->executeQuery($sql); 
    }
  
    public function import()
    {
        $resultData = parent::import();
        $this->processSiteTree();
        return $resultData;
    }
    
    protected function processSiteTree()
    {
        $list = SiteTree::get()->filter(array(
            'T3DataConfigID' => $this->dbConfig->ID
        ));
        
        foreach($list as $page) {
            $this->setParentPage($page);
            if($page instanceof ShortcutPage) {
                $this->setShortcutPage($page);
            }
        }
    }
    
    protected function setParentPage($page)
    {
        if($page->T3_pid && !$page->ParentID) {
            $parent = SiteTree::get()->filter(array(
                'T3_uid' => $page->T3_pid,
                'T3DataConfigID' => $page->T3DataConfigID
             ))->first();
            if($parent) {
                $page->ParentID = $parent->ID;
                $page->write();
                
                if($page->getExistsOnLive()) {
                    $page->publish('Stage', 'Live');
                }
            }
        }
    }
    
    protected function setShortcutPage($page)
    {
        $t3Record = !empty($page->T3_record) ? json_decode($page->T3_record, true) : null;

        if($t3Record && !empty($t3Record['shortcut'])) {
            $linkedPage = SiteTree::get()->filter(array(
                'T3_uid' => $t3Record['shortcut'],
                'T3DataConfigID' => $page->T3DataConfigID
             ))->first();

            if($linkedPage) {
                $page->LinkToID = $linkedPage->ID;
                $page->write();
                
                if($page->getExistsOnLive()) {
                    $page->publish('Stage', 'Live');
                }
            }
        }
    }
    /**
     * 
     * @return array
     */
    public function columnMap()
    {
        return array(
            'starttime' => 'PublishOnDate',
            'endtime' => 'UnPublishOnDate',
            'title' => 'Title',
            'subtitle' => 'SubTitle',
            'nav_title' => 'MenuTitle',
            'description' => 'Description',
            'sorting' => 'Sort',
            'cruser_id' => 'CreatorID',
            'alias' => 'URLSegment',
            'abstract' => 'Abstract',
            'author' => 'AuthorName',
            'author_email' => 'AuthorEmail',
            'lastupdated' => 'DateUpdated',
            'shortcut_mode' => 'ShortcutMode',
            'shortcut' => 'LinkToID',
            'urltype' => 'Protocol',
            'url' => 'ExternalURL',
            'fe_group' => 'CanViewType',
            'nav_hide' => 'ShowInMenus',
            'no_search' => 'ShowInSearch',
            /** @todo 
            'fe_group' => '?',
            'perms_groupid' => '?',
            'is_siteroot' => '?',
             */
        );
 
    }
    
    public function applyMappableFields()
    {
        parent::applyMappableFields();
        
        $this->loader->mappableFields['CreatorID'] = 'CreatorID';
    }
    
    protected function transforms()
    {
        $self = $this;

        return array(
            'PublishOnDate' => array(
                'callback' => function($value,$placeholder) use($self) {
                    return $self->t3Utility->transformTimestamp($value);
                }
            ),
            'UnPublishOnDate' => array(
                'callback' => function($value,$placeholder) use($self) {
                    return $self->t3Utility->transformTimestamp($value);
                }
            ),
            'DateUpdated' => array(
                'callback' => function($value,$placeholder) use($self) {
                    return $self->t3Utility->transformTimestamp($value);
                }
            ),
            'ShortcutMode' => array(
                'callback' => function($value,$placeholder) use($self) {
                    switch($value) {
                        case 1:
                            return 'FirstSubPage';
                            break;
                        case 2:
                            return 'RandomSubPage';
                            break;
                        case 3:
                            return 'Parent';
                            break;
                        case 0:
                        default: 
                            return 'SelectedPage';
                            break;
                    }
                }
            ),
            'LinkToID' => array(
                'callback' => function($value,$placeholder) use($self) {
                    $obj = SiteTree::get()->filter(array(
                        'T3_uid'=> $value,
                        'T3DataConfigID' => $placeholder->T3DataConfigID
                        ))->first();
                    return ($obj) ? $obj->ID : null;
                }
            ),
            'Protocol' => array(
                'callback' => function($value,$placeholder) use($self) {
                    switch($value) {
                        case 2:
                            return 'https://';
                            break;
                        case 3:
                            return 'ftp://';
                            break;
                        case 4:
                            return 'mailto:';
                            break;
                        case 0:
                        case 1:
                        default:
                            return 'http://';
                            break;
                    }
                }
            ),
            'ExternalURL' => array(
                'callback' => function($value,$placeholder) use($self) {
                    // If protocol is set, prepend/replace it
                    if(!empty($placeholder->Protocol)) {
                        $value = preg_replace('/^((http|https|ftp|mailto):(\/\/)?)?/i',$placeholder->Protocol,$value);
                    }
                    return $value;
                }
            ),
            'ShowInMenus' => array(
                'callback' => function($value,$placeholder) use($self) {
                    return !intval($value);
                }
            ),
            'ShowInSearch' => array(
                'callback' => function($value,$placeholder) use($self) {
                    return !intval($value);
                }
            )
        );
        
    }
    
    protected function applyCreateObjectCallback()
    {
        $self = $this;
        // Defaults
        $defaultClass = $this->loader->objectClass;
        $class_uid_map = (array) $this->config()->get('class_uid_map',Config::UNINHERITED);
        $uid_map = [];
        foreach($class_uid_map as $class => $uids) {
            $uids = (array) $uids;
            $uid_map = $uid_map + array_fill_keys($uids,$class);
        }
        
        $this->loader->createObjectCallback = function($record,$defaultClass) use($self,$uid_map) {
            // Check UID map
            if(isset($record['T3_uid']) && !empty($uid_map[$record['T3_uid']])) {
                $class = $uid_map[$record['T3_uid']];
            } 
            // Determine class by doktype
            else {
                // Set ClassName
                switch($record['doktype']) {
                    case 4:
                        $class = 'ShortcutPage';
                        break;
                    case 3:
                        $class = 'ExternalLinkPage';
                        break;
                    case 254:
                    case 255: 
                    case 199:
                        $class = '';    // Unsupported
                        break;
                    case 1:
                    case 6:
                    case 7:
                    default: 
                        $class = $defaultClass;
                        break;
                }
            }

            if(empty($class) || !class_exists($class)) {
                return null;
            }

            return new $class();
        };
    }
    
    
    protected function applyRecordCallback()
    {
        $self = $this;
        
        $this->loader->recordCallback = function($placeholder,$record) use($self) {
            // Set Author to member if found
            if(!empty($record['AuthorEmail'])) {
                $obj = Member::get()->filter(array(
                    'Email' => $record['AuthorEmail']
                ))->first();
                if($obj) {
                    $placeholder->AuthorID = $obj->ID;
                    $placeholder->AuthorName = null;
                    $placeholder->AuthorEmail = null;
                }
            }
            // Custom URLSegment filter
            if(empty($placeholder->URLSegment) && get_class($placeholder) !== 'ExternalLinkPage') {
                $title = strlen((string)$placeholder->MenuTitle) ? $placeholder->MenuTitle : $placeholder->Title;
                if(!empty($title)) {
                    $placeholder->URLSegment = $self->generateURLSegment($title);
                }
                
            }
            
            // Ensure SubsiteID has been set
            if(class_exists('Subsite') && !empty($self->dbConfig->SubsiteID) && empty($obj->SubsiteID)) {
                $obj->SubsiteID = $self->dbConfig->SubsiteID;
            }
        };
    }
    
    protected function applyRecordWrittenCallback()
    {
        /* example  */
        $self = $this;
        
        $this->loader->recordWrittenCallback = function($obj,$record) use($self) {
            // Publish page if not hidden and is current
            $hidden = intval($record['hidden']);
            $current = true;
            if((!empty($record['starttime']) && $record['starttime'] > $self->currentTimestamp)
                || (!empty($record['endtime']) && $record['endtime'] < $self->currentTimestamp)) {
                $current = false;
            }

            if(!$hidden && $current) {
                $obj->publish('Stage', 'Live');
            }
            
            /* Link to editor groups?
            $t3GroupIds = explode(',',$record['perms_groupid']);
            $groups = Group::get()->filter('T3_uid',$t3GroupIds);
            foreach($groups as $group) {
                $obj->EditorGroups()->add($group);
            }
             * 
             */
        };
       
    }
    
    public function generateURLSegment($title){
		$filter = URLSegmentFilter::create();
        $replacements = array_merge(
            Config::inst()->get('URLSegmentFilter','default_replacements'),[
            '/&amp;/u' => '',
            '/&/u' => '',
            '/[_.]+/u' => '',
            '/[.]/u' => '',
            '/[_]/u' => '-',
        ]);
        $filter->setReplacements($replacements);
        
		$t = $filter->filter($title);
		// Fallback to generic page name if path is empty (= no valid, convertable characters)
		if(!$t || $t == '-' || $t == '-1') $t = "page-$this->ID";

		return $t;
	}
    
}
