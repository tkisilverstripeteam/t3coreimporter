<?php


class T3FileImporter extends T3Importer {
    
    
    private static $object_class = 'File';
    
    private static $default_folder = 'imported';
    
    protected $t3Table = 'sys_file';
    
    protected $skipImported = true;
    
    public function doSourceQuery($skipDeleted=true,$skipImported=false)
    {
        
        $sql = "SELECT sys_file.*, m.title, m.description, m.alternative FROM sys_file";
        $sql .= ' JOIN sys_file_metadata as m ON sys_file.uid=m.file';

        if($skipImported) {
            $class = Config::inst()->get(get_class($this),'object_class',Config::UNINHERITED);
            $importedUids = array_unique(DataObject::get($class)->filter([
               'T3DataConfigID' => $this->dbConfig->ID 
            ])->column('T3_uid'));
            if(!empty($importedUids)) {
                $sql = $this->dbConnector->addWhere($sql, "{$this->t3Table}.uid NOT IN (". implode(',',$importedUids)) .')';
            }
        }
        
        return $this->dbConnector->executeQuery($sql); 
    }
    
    /**
     * 
     * @return array
     */
    public function columnMap()
    {
        return array(
            'identifier' => 'Filename',
            'name' => 'Title',
            'description' => 'Description',
            'creation_date' => 'Created'
        );
 
    }
    
    protected function applyCreateObjectCallback()
    {
        $self = $this;
        // Defaults
        $defaultClass = $this->loader->objectClass;
        
        $this->loader->createObjectCallback = function($record) use($self,$defaultClass) {
            // Set image class
            if(strpos($record['mime_type'],'image') === 0) {
                $class = 'Image';
            } else {
                $class = $defaultClass;
            }
            
            if(empty($class) || !class_exists($class)) {
                return null;
            }

            return new $class();
        };
    }
    
    protected function applyRecordCallback()
    {
        $self = $this;
        // Create file path
        $this->loader->recordCallback = function($obj,$record) use($self) {
            // Alter file name filter
            Config::inst()->update('FileNameFilter','default_replacements',array(
                '/\s/' => '-', // remove whitespace
                '/_/' => '_', // retain underscores
                '/[^A-Za-z0-9+.\-_]+/' => '', // remove non-ASCII chars, only allow alphanumeric plus dash,dot, and underscore
                '/[\-]{2,}/' => '-', // remove duplicate dashes
                '/^[\.\-]+/' => '', // Remove all leading dots, dashes, not underscores
                
            ));
            // Create all folders in path and set parent folder of item
            $path = dirname($obj->Filename);
            $path = str_replace(ASSETS_DIR,'',$path);
            $parent = Folder::find_or_make($path);
            if($parent) {
                $obj->ParentID = $parent->ID;
            }
            
            // Ensure SubsiteID has been set
            if(class_exists('Subsite') && !empty($self->dbConfig->SubsiteID) && empty($obj->SubsiteID)) {
                $obj->SubsiteID = $self->dbConfig->SubsiteID;
            }
        };
   
    }
    
    public function applyMappableFields()
    {
        parent::applyMappableFields();
        $this->loader->mappableFields['Title'] = 'Title';
    }
    
    protected function duplicateChecks()
    {
        return array(
            'Filename'
        );
    }
    
    protected function transforms()
    {
        $self = $this;
        // File folder setup
        $fileFolder = $this->dbConfig->FileFolder ?: Config::inst()->get(get_class($this),'default_folder');
        if(!empty($fileFolder)) {
            Folder::find_or_make($fileFolder);
        }
        
        return array(
            'Filename' => array(
                'callback' => function($value,$placeholder) use($self,$fileFolder) {
                    // Set Name automatically by Filename, otherwise 
                    // setting/updating Name overrides Filename and causes issues
                    $placeholder->Name = null;  
                    return $fileFolder . $value;
                }
            )
        );
    }
    
    /**
     * Import
     */
    public function clear($filters=[])
    {
        // Set execution time
        $limit = intval(Config::inst()->get(get_class($this),'time_limit') ?: 600);
        set_time_limit($limit);
        
        // Run
        try {
            $class = Config::inst()->get(get_class($this),'object_class',Config::UNINHERITED);
            
            $classes = ClassInfo::ancestry($class,true);
            $baseClass = array_shift($classes);
            $versioned = Object::has_extension($baseClass,'Versioned');

            $queryFilters = [
               'T3DataConfigID' => $this->dbConfig->ID
            ];
            
            if(class_exists('Subsite') && $this->dbConfig->SubsiteID) {
                $queryFilters['SubsiteID'] = $this->dbConfig->SubsiteID;
            }
            
            if(is_array($filters)) {
                $queryFilters += $filters;
            }
                
            $query = SQLDelete::create()
                ->setFrom('"'. $class .'"')
                ->setWhere($queryFilters);
            $query->execute();
            $deleted = DB::affected_rows();
            $resultData = array(
                'output' => sprintf("%d records deleted",$deleted)
            );
        } catch (Exception $ex) {
            $resultData = array(
                'error' => $ex->getMessage()
            );
        }
        
        return $resultData;
    }
    
    
}
